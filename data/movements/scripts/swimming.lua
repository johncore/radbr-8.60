local outfit = {lookType = 267, lookHead = 0, lookBody = 0, lookLegs = 0, lookFeet = 0, lookTypeEx = 0, lookAddons = 0}


function onStepIn(cid, item, position, lastPosition, fromPosition, toPosition, actor)
if(not isPlayer(cid)) then
return true
end
if(not isPlayerGhost(cid)) then
end
doSetCreatureOutfit(cid, outfit, -1)
return true
end
	
function onStepOut(cid, item, position, lastPosition, fromPosition, toPosition, actor)
if(not isPlayer(cid)) then
return true
end
if(not isPlayerGhost(cid)) then
end
doRemoveCondition(cid, CONDITION_OUTFIT)
return true
end
