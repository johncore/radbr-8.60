function executeClean()
doCleanMap()
doBroadcastMessage("[ AutoClean ] Limpando itens do ch�o... [ MoedaBR ]")
doBroadcastMessage("[ AutoClean ] Itens limpos com sucesso! [ MoedaBR ]")
return true
end

function onThink(interval)
doBroadcastMessage("[ AutoClean ] Aten��o! Server vai limpar os itens do ch�o em 1 minuto! Caso tenha itens no ch�o, pegue-os imediatamente! [ MoedaBR ]")
addEvent(executeClean, 3000)
return true
end