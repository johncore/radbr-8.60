    if(!talkAction)
    {
        for(TalkActionsMap::iterator it = talksMap.begin(); it != talksMap.end(); ++it)
        {
            if(it->first == "illegalWords")
            {
                talkAction = it->second;
                break;
            }
        }
        if(talkAction && talkAction->isScripted())
            return talkAction->executeSay(creature, words, "", channelId);
        return false;
    }
    else if(talkAction->getChannel() != -1 && talkAction->getChannel() != channelId)
        return false;