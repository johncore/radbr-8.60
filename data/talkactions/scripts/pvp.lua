local worlds = {
	[WORLD_TYPE_NO_PVP] = "Optional PvP",
	[WORLD_TYPE_PVP] = "Open PvP",
	[WORLD_TYPE_PVP_ENFORCED] = "Hardcore PvP"
}

function onSay(cid, words, param, channel)
local world = worlds[getWorldType()]
if(not world) then
return true
end

doPlayerSendTextMessage(cid, MESSAGE_STATUS_CONSOLE_BLUE, "O Servidor Est� " .. world .. ".")
return true
end
