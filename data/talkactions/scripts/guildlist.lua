function onSay(cid, words, param, channel)
	if param == '' then
		local list = db.getResult("SELECT `name` FROM `guilds`;")
		if(list:getID() ~= -1) then
			local v = ''
			repeat
				v = v .. list:getDataString("name")  .. "\n"
			until not list:next()
			list:free()
			doShowTextDialog(cid, 9969, v)
		else
			doPlayerSendTextMessage(cid, MESSAGE_STATUS_CONSOLE_BLUE, "N�o existem guilds no server.")
		end
	else
		local id = getGuildId(param)
		if id then
			local list = db.getResult("SELECT `name`, `rank_id` FROM `players` WHERE `rank_id` IN (SELECT `id` FROM `guild_ranks` WHERE `guild_id` = " .. id .. ");")
			if(list:getID() ~= -1) then
				local v = ''
				repeat
					local rank = db.getResult("SELECT `name` FROM `guild_ranks` WHERE `id` = " .. list:getDataInt("rank_id") .. " LIMIT 1;")
					v = v .. list:getDataString("name")  .. " [" .. rank:getDataString("name") .. "]\n"
					rank:free()
				until not list:next()
				list:free()
				doShowTextDialog(cid, 9969, v)
			else
				doPlayerSendTextMessage(cid, MESSAGE_STATUS_CONSOLE_BLUE, "N�o h� players nesta guild.")
			end
		else
			doPlayerSendTextMessage(cid, MESSAGE_STATUS_CONSOLE_BLUE, "Esta guild n�o existe.")
		end
	end
	return true
end