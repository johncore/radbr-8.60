local montConfig = 
{
	['Widow Queen'] 		= {cost = 300000, id = 1},
	['Racing Bird'] 		= {cost = 300000, id = 2},
	['War Bear'] 		= {cost = 300000, id = 3},
	['Black Sheep'] 		= {cost = 300000, id = 4},
	['Midnight Panther'] 	= {cost = 300000, id = 5},
	['Draptor'] 		= {cost = 300000, id = 6},
	['Titanica'] 		= {cost = 300000, id = 7},
	['Tin Lizard'] 		= {cost = 300000, id = 8},
	['Blazebringer'] 		= {cost = 300000, id = 9},
	['Rapid Boar'] 		= {cost = 300000, id = 10},
	['Stampor'] 		= {cost = 300000, id = 11},
	['Undead Cavebear'] 	= {cost = 300000, id = 12}
}

function onSay(cid, words, param)
	if(param == '') then
		local str = ""
		for name, options in pairs(montConfig) do
			str = str .. "\n" .. name
		end

		doPlayerPopupFYI(cid, "Lista de montarias:\n" .. str)
		return true
	end

	local mount = montConfig[param]
	if(mount ~= nil) then
		if(not doPlayerRemoveMoney(cid, mount.cost)) then
			doPlayerSendCancel(cid, "Voc� n�o tem dinheiro suficiente para comprar esta montaria.")
			doSendMagicEffect(getCreaturePosition(cid), CONST_ME_POFF)
			return true
		end

		doPlayerAddMount(cid, mount.id)
		doSendMagicEffect(getCreaturePosition(cid), CONST_ME_GIFT_WRAPS)
	else
		doPlayerSendCancel(cid, 'A montaria desejada n�o existe na lista. Use "!mount" para ver a lista de montarias.')
	end
	return true
end