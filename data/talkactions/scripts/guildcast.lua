function onSay(cid, words, param)
	local playerGuild = getPlayerGuildId(cid)
	if playerGuild > 0 then
		local playerGuildLevel = getPlayerGuildLevel(cid)
		if playerGuildLevel >= GUILDLEVEL_VICE then
			local players = getOnlinePlayers()
			local message = "*Guild* " .. getCreatureName(cid) .. " [" .. getPlayerLevel(cid) .. "]:\n" .. param;
			for i,playerName in ipairs(players) do
				local player = getPlayerByName(playerName);
				if getPlayerGuildId(player) == playerGuild then
					doPlayerSendTextMessage(player, MESSAGE_STATUS_WARNING, message);
				end
			end
			doPlayerSendCancel(cid, "Mensagem para os membros da Guild.");
		else
			doPlayerSendCancel(cid, "Voc� precisa ser pelo menos vice-l�der para falar no guildcast.");
		end
	else
		doPlayerSendCancel(cid, "Desculpe, voc� n�o � de guild.");
	end
	doPlayerSendTextMessage(cid, 23, words)
	return FALSE
end
