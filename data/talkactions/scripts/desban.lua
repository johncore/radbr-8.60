function onSay(cid, words, param, channel)

if (getTilePzInfo(getCreaturePosition(cid)) == FALSE) then
doPlayerSendTextMessage(cid,MESSAGE_STATUS_CONSOLE_BLUE,"Voc� precisa est� em �rea protegida para utilizar este comando.")
return true
end


if(param == '') then
doPlayerSendTextMessage(cid, MESSAGE_STATUS_CONSOLE_BLUE, "Digite o nome do personagem que deseja desbanir.")
return true
end

local account, tmp = getAccountIdByName(param), true
if(account == 0) then
account = getAccountIdByAccount(param)
if(account == 0) then
doPlayerSendTextMessage(cid, MESSAGE_STATUS_CONSOLE_BLUE, "O jogador " .. param .. " n�o existe.")
return true
end
tmp = false
end


local ban = getBanData(account, BAN_ACCOUNT)
if(ban and doRemoveAccountBanishment(account)) then
local name = param
if(tmp) then
name = account
end
doPlayerSendTextMessage(cid, MESSAGE_STATUS_CONSOLE_BLUE, "Jogador " .. param .. " foi " .. (ban.expires == -1 and "desbanido" or "desbanido") .. " com sucesso.")
end

if getPlayerItemCount(cid, 9971) >= 20 then
doPlayerRemoveItem(cid, 9971, 20)
return true
else
doPlayerSendTextMessage(cid, MESSAGE_STATUS_CONSOLE_BLUE, "Voc� n�o tem barras de ouro suficiente. � necess�rio 20 barras de ouro para desbanir o jogador " .. param .. ".")
return true
end

if(not tmp) then
return true
end

tmp = getIpByName(param)
if(isIpBanished(tmp) and doRemoveIpBanishment(tmp)) then
doPlayerSendTextMessage(cid, MESSAGE_STATUS_CONSOLE_BLUE, "IP Banishment on " .. doConvertIntegerToIp(ip) .. " has been lifted.")
end

local guid = getPlayerGUIDByName(param, true)
if(guid == nil) then
return true
end

ban = getBanData(guid, BAN_PLAYER, PLAYERBAN_LOCK)
if(ban and doRemovePlayerBanishment(guid, PLAYERBAN_LOCK)) then
doPlayerSendTextMessage(cid, MESSAGE_STATUS_CONSOLE_BLUE, "Namelock from " .. param .. " has been removed.")
end

ban = getBanData(guid, BAN_PLAYER, PLAYERBAN_BANISHMENT)
if(ban and doRemovePlayerBanishment(guid, PLAYERBAN_BANISHMENT)) then
doPlayerSendTextMessage(cid, MESSAGE_STATUS_CONSOLE_BLUE, param .. " has been " .. (ban.expires == -1 and "undeleted" or "unbanned") .. ".")
end
return true
end
