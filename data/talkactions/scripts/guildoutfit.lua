  local config = {
        exhaustionInSeconds = 10,
        storage = 34534
}

function onSay(cid, words, param)
        if(exhaustion.check(cid, config.storage) == TRUE) then
                doPlayerSendCancel(cid, "� necess�rio esperar mais " .. config.exhaustionInSeconds .. " segundos para fazer a troca novamente.")
                return TRUE
        end

        local playerGuild = getPlayerGuildId(cid)
        if(playerGuild == FALSE) then
                doPlayerSendCancel(cid, "Desculpe, voc� n�o pertence a nenhuma Guild.")
                return TRUE
        end

        local playerGuildLevel = getPlayerGuildLevel(cid)
        if(playerGuildLevel < GUILDLEVEL_VICE) then
                doPlayerSendCancel(cid, "Voc� precisa ser l�der de uma Guild para mudar os outfits.")
                return TRUE
        end

        local players = getPlayersOnline()
        local outfit = getCreatureOutfit(cid)
        local message = "*Guild* Seu outfit foi mudado pelo l�der da sua guild. (" .. getCreatureName(cid) .. ")"
        local members = 0
        local tmp = {}
        for i, tid in ipairs(players) do
                if(getPlayerGuildId(tid) == playerGuild and cid ~= tid) then
                        tmp = outfit
                        if(canPlayerWearOutfit(tid, outfit.lookType, outfit.lookAddons) ~= TRUE) then
                                local tidOutfit = getCreatureOutfit(tid)
                                tmp.lookType = tidOutfit.lookType
                                tmp.lookAddons = tidOutfit.lookAddons
                        end

                        doSendMagicEffect(getCreaturePosition(tid), 66)
                        doCreatureChangeOutfit(tid, tmp)
                        doPlayerSendTextMessage(tid, MESSAGE_INFO_DESCR, message)
                        members = members + 1
                end
        end

        exhaustion.set(cid, config.storage, config.exhaustionInSeconds)
        doPlayerSendCancel(cid, "As roupas dos Membros da Guild foram trocadas. (Total: " .. members .. ")")
        return TRUE
end