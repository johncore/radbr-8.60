function onSay(cid, words, param)
 
if (getTilePzInfo(getCreaturePosition(cid)) == FALSE) then
doPlayerSendTextMessage(cid, MESSAGE_STATUS_CONSOLE_BLUE,"Voc� precisa est� em �rea protegida para utilizar este comando.")
return TRUE
end

if param == "" then
doPlayerSendTextMessage(cid, MESSAGE_STATUS_CONSOLE_BLUE, "Por favor coloque a quantidade que deseja depositar.")
return TRUE
end

if (getPlayerStorageValue(cid, 11548) <= os.time()) then
if math.abs(tonumber(param)) <= getPlayerItemCount(cid, 9971) then
doPlayerRemoveItem(cid, 9971, param)
doPlayerAddPoints(cid, param)
doPlayerSendTextMessage(cid, MESSAGE_INFO_DESCR, "Parab�ns, voc� depositou " .. param .. " barras de ouro com sucesso! Seu saldo no banco agora � de " .. getPlayerPoints(cid) .. " moedas.")
setPlayerStorageValue(cid,11548,os.time()+30)
return TRUE
else
doPlayerSendTextMessage(cid, MESSAGE_INFO_DESCR, "Voc� n�o possui a quantidade desejada para depositar.")
setPlayerStorageValue(cid,11548,os.time()+30)
return TRUE
end
else
doPlayerSendTextMessage(cid, MESSAGE_STATUS_CONSOLE_RED,"Por medidas de seguran�a voc� s� pode utilizar este comando em " .. (getPlayerStorageValue(cid, 11548)-os.time()+(0)) .. " segundos.")
return TRUE
end
end