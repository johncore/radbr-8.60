function onSay(cid, words, param)

if (getTilePzInfo(getCreaturePosition(cid)) == FALSE) then
doPlayerSendTextMessage(cid, MESSAGE_STATUS_CONSOLE_BLUE,"Voc� precisa est� em �rea protegida para utilizar este comando.")
return TRUE
end

if (getPlayerStorageValue(cid, 11548) >= os.time()) then
doPlayerSendTextMessage(cid, MESSAGE_STATUS_CONSOLE_RED,"Por medidas de seguran�a voc� s� pode utilizar este comando em " .. (getPlayerStorageValue(cid, 11548)-os.time()+(0)) .. " segundos.")
return TRUE
end


if(param ~= "") and (param ~= "sd") and (param ~= "uh")  and (param ~= "Stamina Up") and (param ~= "explo") and (param ~= "vip10") and (param ~= "vip30") and (param ~= "super divine axe") and (param ~= "super divine club") and (param ~= "super divine sword") and (param ~= "super divine crossbow") and (param ~= "livro nivel 6") and (param ~= "super divine staff") and (param ~= "skillclub") and (param ~= "skillsword") and (param ~= "skillaxe") and (param ~= "skilldistance") and (param ~= "skillshielding") and (param ~= "magiclevel") and (param ~= "magiclevel5") and (param ~= "skillclub10") and (param ~= "skillsword10") and (param ~= "skillaxe10") and (param ~= "skilldistance10") and (param ~= "skillshielding10") and (param ~= "removerfrag") and (param ~= "novark") then
doPlayerSendTextMessage(cid, MESSAGE_STATUS_CONSOLE_BLUE, "Para comprar digite !comprar (nome do item)\nOpcoes:\nsd = 4000 em SD por 70 barras.\nuh = 6000 em UH por 70 barras.\nexplo = 6000 em explosion por 10 barras.\nstamina up = Stamina Up por 100 barras.\nvip10 = 10 dias de vip por 5 barras.\nvip30 = 30 dias de vip por 10 barras.\ndivine staff = divine staff por 30 barras.\ndivine axe = divine axe por 30 barras.\nlivro nivel 6 = livro nivel 6 por 60 barras.\ndivine club = divine club por 30 barras.\ndivine sword = divine sword por 30 barras.\ndivine crossbow = divine crossbow por 30 barras.\nlivro nivel 5 = livro nivel 5 por 30 barras.")
doPlayerSendTextMessage(cid, MESSAGE_STATUS_CONSOLE_BLUE, "\nsuper divine axe = super divine axe por 60 barras.\nsuper divine club = super divine club por 60 barras.\nsuper divine sword = super divine sword por 60 barras.\nsuper divine staff = super divine staff por 60 barras.\nsuper divine crossbow = super divine crossbow por 60 barras.\nskillclub = adiciona 1 skill club por 1 barras.\nskillsword = adiciona 1 skill sword por 1 barras.\nskillaxe = adiciona 1 skill axe por 1 barras.\nskilldistance = adiciona 1 skill distance por 1 barras.")
doPlayerSendTextMessage(cid, MESSAGE_STATUS_CONSOLE_BLUE, "\nskillshielding = adiciona 1 skill shielding por 1 barras.\nmagiclevel = adiciona 1 magic level por 3 barras.\nmagiclevel5 = adiciona 5 magic level por 15 barras.\nskillclub10 = adiciona 10 skills club por 10 barras.\nskillsword10 = adiciona 10 skills sword por 10 barras.\nskillaxe10 = adiciona 10 skills axe por 10 barras.\nskilldistance10 = adiciona 10 skill distance por 10 barras.\nskillshielding10 = adiciona 10 skill shielding por 10 barras.\nremoverfrag = remove todos frags por 100k.")
doPlayerSendTextMessage(cid, MESSAGE_STATUS_CONSOLE_BLUE, "O Item desejado n�o existe em nosso stock. Veja a cima os detalhes dos items dispon�veis.")
return TRUE
end

if(param == "") then
doPlayerSendTextMessage(cid, MESSAGE_STATUS_CONSOLE_BLUE, "Para comprar digite !comprar (nome do item)\nOpcoes:\nsd = 4000 em SD por 50 barras.\nuh = 6000 em UH por 40 barras.\nexplo = 6000 em explosion por 10 barras.\nstamina up = Stamina Up por 100 barras.\nvip10 = 10 dias de vip por 5 barras.\nvip30 = 30 dias de vip por 10 barras.\ndivine staff = divine staff por 30 barras.\ndivine axe = divine axe por 30 barras.\ndivine club = divine club por 30 barras.\ndivine sword = divine sword por 30 barras.\ndivine crossbow = divine crossbow por 30 barras.\nlivro nivel 5 = livro nivel 5 por 30 barras.")
doPlayerSendTextMessage(cid, MESSAGE_STATUS_CONSOLE_BLUE, "\nsuper divine axe = super divine axe por 60 barras.\nsuper divine staff = super divine staff por 60 barras.\nsuper divine club = super divine club por 60 barras.\nsuper divine sword = super divine sword por 60 barras.\nsuper divine crossbow = super divine crossbow por 60 barras.\nlivro nivel 6 = livro nivel 6 por 60 barras.\nskillclub = adiciona 1 skill club por 1 barras.\nskillsword = adiciona 1 skill sword por 1 barras.\nskillaxe = adiciona 1 skill axe por 1 barras.\nskilldistance = adiciona 1 skill distance por 1 barras.")
doPlayerSendTextMessage(cid, MESSAGE_STATUS_CONSOLE_BLUE, "\nskillshielding = adiciona 1 skill shielding por 1 barras.\nmagiclevel = adiciona 1 magic level por 3 barras.\nmagiclevel5 = adiciona 5 magic level por 15 barras.\nskillclub10 = adiciona 10 skills club por 10 barras.\nskillsword10 = adiciona 10 skills sword por 10 barras.\nskillaxe10 = adiciona 10 skills axe por 10 barras.\nskilldistance10 = adiciona 10 skill distance por 10 barras.\nskillshielding10 = adiciona 10 skill shielding por 10 barras.\nremoverfrag = remove um frag por 100k.")
doPlayerSendTextMessage(cid, MESSAGE_STATUS_CONSOLE_BLUE, "\nnovark = gera uma nova RK para sua account por 10 barras.\n")
return TRUE
end

-------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
if(param == "sd") then
if getPlayerItemCount(cid, 9971) >= 70 then
local bag = doPlayerAddItem(cid, 5926, 1)
doAddContainerItem(bag, 2268, 2000)
local bag = doPlayerAddItem(cid, 5926, 1)
doAddContainerItem(bag, 2268, 2000)
doPlayerRemoveItem(cid, 9971, 70)
doPlayerSendTextMessage(cid, MESSAGE_STATUS_CONSOLE_BLUE, "Parab�ns voc� comprou 4k de SD com sucesso.")
setPlayerStorageValue(cid,11548,os.time()+30)
return TRUE
else
doPlayerSendTextMessage(cid, MESSAGE_STATUS_CONSOLE_BLUE, "Voc� n�o possui a quantidade necess�ria para comprar.")
return TRUE
end
end

-------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
if(param == "uh") then
if getPlayerItemCount(cid, 9971) >= 70 then
local bag = doPlayerAddItem(cid, 2002, 1)
doAddContainerItem(bag, 2273, 2000)
local bag = doPlayerAddItem(cid, 2002, 1)
doAddContainerItem(bag, 2273, 2000)
local bag = doPlayerAddItem(cid, 2002, 1)
doAddContainerItem(bag, 2273, 2000)
doPlayerRemoveItem(cid, 9971, 70)
doPlayerSendTextMessage(cid, MESSAGE_STATUS_CONSOLE_BLUE, "Parab�ns voc� comprou 6k de UH com sucesso.")
setPlayerStorageValue(cid,11548,os.time()+10)
return TRUE
else
doPlayerSendTextMessage(cid, MESSAGE_STATUS_CONSOLE_BLUE, "Voc� n�o possui a quantidade necess�ria para comprar.")
return TRUE
end
end

-------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
if(param == "explo") then
if getPlayerItemCount(cid, 9971) >= 10 then
local bag = doPlayerAddItem(cid, 2001, 1)
doAddContainerItem(bag, 2313, 2000)
local bag = doPlayerAddItem(cid, 2001, 1)
doAddContainerItem(bag, 2313, 2000)
local bag = doPlayerAddItem(cid, 2001, 1)
doAddContainerItem(bag, 2313, 2000)
doPlayerRemoveItem(cid, 9971, 10)
doPlayerSendTextMessage(cid, MESSAGE_STATUS_CONSOLE_BLUE, "Parab�ns voc� comprou 6k de Explosion com sucesso.")
setPlayerStorageValue(cid,11548,os.time()+30)
return TRUE
else
doPlayerSendTextMessage(cid, MESSAGE_STATUS_CONSOLE_BLUE, "Voc� n�o possui a quantidade necess�ria para comprar.")
return TRUE
end
end

-------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
if(param == "Stamina Up") then
if getPlayerItemCount(cid, 9971) >= 100 then
local bag = doPlayerAddItem(cid, 1997, 1)
local new_item = doAddContainerItem(bag, 12505, 1)
doItemSetAttribute(new_item, "description", "Este item pode ser adquirido atrav�s do shopping. Adquirido dia " .. os.date("%d/%m/%Y - %X") .." por ".. getPlayerName(cid) ..". Serial: "..  getPlayerGUID(cid) ..".")
doPlayerRemoveItem(cid, 9971, 100)
doPlayerSendTextMessage(cid, MESSAGE_STATUS_CONSOLE_BLUE, "Voc� comprou uma Stamina UP com sucesso.")
setPlayerStorageValue(cid,11548,os.time()+30)
return TRUE
else
doPlayerSendTextMessage(cid, MESSAGE_STATUS_CONSOLE_BLUE, "Voc� n�o possui a quantidade necess�ria para comprar.")
return TRUE
end
end

-------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
if(param == "vip10") then
if getPlayerItemCount(cid, 9971) >= 5 then
doPlayerAddPremiumDays(cid, 10)
doPlayerRemoveItem(cid, 9971, 5)
doPlayerSendTextMessage(cid, MESSAGE_STATUS_CONSOLE_BLUE, "Parab�ns voc� comprou 10 dias de vip com sucesso.")
setPlayerStorageValue(cid,11548,os.time()+30)
return TRUE
else
doPlayerSendTextMessage(cid, MESSAGE_STATUS_CONSOLE_BLUE, "Voc� n�o possui a quantidade necess�ria para comprar.")
return TRUE
end
end

-------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
if(param == "vip30") then
if getPlayerItemCount(cid, 9971) >= 10 then
doPlayerAddPremiumDays(cid, 30)
doPlayerRemoveItem(cid, 9971, 10)
doPlayerSendTextMessage(cid, MESSAGE_STATUS_CONSOLE_BLUE, "Parab�ns voc� comprou 30 dias de vip com sucesso.")
setPlayerStorageValue(cid,11548,os.time()+30)
return TRUE
else
doPlayerSendTextMessage(cid, MESSAGE_STATUS_CONSOLE_BLUE, "Voc� n�o possui a quantidade necess�ria para comprar.")
return TRUE
end
end

-------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
if(param == "super divine axe") then
if getPlayerItemCount(cid, 9971) >= 60 then
local bag = doPlayerAddItem(cid, 1997, 1)
local new_item = doAddContainerItem(bag, 8926, 1)
doItemSetAttribute(new_item, "description", "Este item pode ser adquirido atrav�s do shopping. Adquirido dia " .. os.date("%d/%m/%Y - %X") .." por ".. getPlayerName(cid) ..". Serial: "..  getPlayerGUID(cid) ..".")
doPlayerRemoveItem(cid, 9971, 60)
doPlayerSendTextMessage(cid, MESSAGE_STATUS_CONSOLE_BLUE, "Voc� comprou um super divine axe com sucesso.")
setPlayerStorageValue(cid,11548,os.time()+30)
return TRUE
else
doPlayerSendTextMessage(cid, MESSAGE_STATUS_CONSOLE_BLUE, "Voc� n�o possui a quantidade necess�ria para comprar.")
return TRUE
end
end

-------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
if(param == "super divine staff") then
if getPlayerItemCount(cid, 9971) >= 60 then
local bag = doPlayerAddItem(cid, 1997, 1)
local new_item = doAddContainerItem(bag, 8922, 1)
doItemSetAttribute(new_item, "description", "Este item pode ser adquirido atrav�s do shopping. Adquirido dia " .. os.date("%d/%m/%Y - %X") .." por ".. getPlayerName(cid) ..". Serial: "..  getPlayerGUID(cid) ..".")
doPlayerRemoveItem(cid, 9971, 60)
doPlayerSendTextMessage(cid, MESSAGE_STATUS_CONSOLE_BLUE, "Voc� comprou um super divine staff com sucesso.")
setPlayerStorageValue(cid,11548,os.time()+30)
return TRUE
else
doPlayerSendTextMessage(cid, MESSAGE_STATUS_CONSOLE_BLUE, "Voc� n�o possui a quantidade necess�ria para comprar.")
return TRUE
end
end

-------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
if(param == "super divine club") then
if getPlayerItemCount(cid, 9971) >= 60 then
local bag = doPlayerAddItem(cid, 1997, 1)
local new_item = doAddContainerItem(bag, 7423, 1)
doItemSetAttribute(new_item, "description", "Este item pode ser adquirido atrav�s do shopping. Adquirido dia " .. os.date("%d/%m/%Y - %X") .." por ".. getPlayerName(cid) ..". Serial: "..  getPlayerGUID(cid) ..".")
doPlayerRemoveItem(cid, 9971, 60)
doPlayerSendTextMessage(cid, MESSAGE_STATUS_CONSOLE_BLUE, "Voc� comprou um super divine club com sucesso.")
setPlayerStorageValue(cid,11548,os.time()+30)
return TRUE
else
doPlayerSendTextMessage(cid, MESSAGE_STATUS_CONSOLE_BLUE, "Voc� n�o possui a quantidade necess�ria para comprar.")
return TRUE
end
end

-------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
if(param == "super divine sword") then
if getPlayerItemCount(cid, 9971) >= 60 then
local bag = doPlayerAddItem(cid, 1997, 1)
local new_item = doAddContainerItem(bag, 7403, 1)
doItemSetAttribute(new_item, "description", "Este item pode ser adquirido atrav�s do shopping. Adquirido dia " .. os.date("%d/%m/%Y - %X") .." por ".. getPlayerName(cid) ..". Serial: "..  getPlayerGUID(cid) ..".")
doPlayerRemoveItem(cid, 9971, 60)
doPlayerSendTextMessage(cid, MESSAGE_STATUS_CONSOLE_BLUE, "Voc� comprou um super divine sword com sucesso.")
setPlayerStorageValue(cid,11548,os.time()+30)
return TRUE
else
doPlayerSendTextMessage(cid, MESSAGE_STATUS_CONSOLE_BLUE, "Voc� n�o possui a quantidade necess�ria para comprar.")
return TRUE
end
end

-------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
if(param == "super divine crossbow") then
if getPlayerItemCount(cid, 9971) >= 60 then
local bag = doPlayerAddItem(cid, 1997, 1)
local new_item = doAddContainerItem(bag, 8851, 1)
doItemSetAttribute(new_item, "description", "Este item pode ser adquirido atrav�s do shopping. Adquirido dia " .. os.date("%d/%m/%Y - %X") .." por ".. getPlayerName(cid) ..". Serial: "..  getPlayerGUID(cid) ..".")
doPlayerRemoveItem(cid, 9971, 60)
doPlayerSendTextMessage(cid, MESSAGE_STATUS_CONSOLE_BLUE, "Voc� comprou um super divine crossbow com sucesso.")
setPlayerStorageValue(cid,11548,os.time()+30)
return TRUE
else
doPlayerSendTextMessage(cid, MESSAGE_STATUS_CONSOLE_BLUE, "Voc� n�o possui a quantidade necess�ria para comprar.")
return TRUE
end
end

-------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
if(param == "livro nivel 6") then
if getPlayerItemCount(cid, 9971) >= 60 then
local bag = doPlayerAddItem(cid, 1997, 1)
local new_item = doAddContainerItem(bag, 8921, 1)
doItemSetAttribute(new_item, "description", "Este item pode ser adquirido atrav�s do shopping. Adquirido dia " .. os.date("%d/%m/%Y - %X") .." por ".. getPlayerName(cid) ..". Serial: "..  getPlayerGUID(cid) ..".")
doPlayerRemoveItem(cid, 9971, 60)
doPlayerSendTextMessage(cid, MESSAGE_STATUS_CONSOLE_BLUE, "Voc� comprou um livro nivel 6 com sucesso.")
setPlayerStorageValue(cid,11548,os.time()+30)
return TRUE
else
doPlayerSendTextMessage(cid, MESSAGE_STATUS_CONSOLE_BLUE, "Voc� n�o possui a quantidade necess�ria para comprar.")
return TRUE
end
end

-------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
if(param == "magiclevel") then
if getPlayerMagLevel(cid) >= 200 then
doPlayerSendTextMessage(cid, MESSAGE_STATUS_CONSOLE_BLUE, "Voc� n�o pode ter magic level acima de 200.")
return TRUE
end
if(not isSorcerer(cid) and not isDruid(cid) and not isInfernalist(cid)) then
doPlayerSendTextMessage(cid, MESSAGE_STATUS_CONSOLE_BLUE, "Somente Sorcerers, Druids e Infernalists podem comprar magic level.")
return TRUE
end
if getPlayerItemCount(cid, 9971) >= 3 then
local pid = getPlayerGUID(cid) 
doPlayerRemoveItem(cid, 9971, 3)
setPlayerStorageValue(cid,11548,os.time()+0)
doRemoveCreature(cid)
db.executeQuery("UPDATE `players` SET `maglevel` = `maglevel` + 1 WHERE `id` = "..pid)
return TRUE
else
doPlayerSendTextMessage(cid, MESSAGE_STATUS_CONSOLE_BLUE, "Voc� n�o possui a quantidade necess�ria para comprar.")
return TRUE
end
end

-------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
if(param == "skillclub") then
if getPlayerSkillLevel(cid, SKILL_CLUB) >= 350 then
doPlayerSendTextMessage(cid, MESSAGE_STATUS_CONSOLE_BLUE, "Voc� n�o pode ter skill acima de 350.")
return TRUE
end
if not isDrunou(cid) then
doPlayerSendTextMessage(cid, MESSAGE_STATUS_CONSOLE_BLUE, "Somente Drunous podem comprar skill de club.")
return TRUE
end
if getPlayerItemCount(cid, 9971) >= 1 then
local pid = getPlayerGUID(cid)
local club = getPlayerSkillLevel(cid, SKILL_CLUB) 
doPlayerRemoveItem(cid, 9971, 1)
setPlayerStorageValue(cid,11548,os.time()+0)
doRemoveCreature(cid)
db.executeQuery("UPDATE `player_skills` SET `value` = " .. (club + 1) .. ", `count` = 0 WHERE `skillid` = 1 and `player_id` = " .. pid .. ";")
return TRUE
else
doPlayerSendTextMessage(cid, MESSAGE_STATUS_CONSOLE_BLUE, "Voc� n�o possui a quantidade necess�ria para comprar.")
return TRUE
end
end

-------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
if(param == "skillsword") then
if getPlayerSkillLevel(cid, SKILL_SWORD) >= 350 then
doPlayerSendTextMessage(cid, MESSAGE_STATUS_CONSOLE_BLUE, "Voc� n�o pode ter skill acima de 350.")
return TRUE
end
if not isKnight(cid) then
doPlayerSendTextMessage(cid, MESSAGE_STATUS_CONSOLE_BLUE, "Somente Knights podem comprar skill de sword.")
return TRUE
end
if getPlayerItemCount(cid, 9971) >= 1 then
local pid = getPlayerGUID(cid)
local sword = getPlayerSkillLevel(cid, SKILL_SWORD) 
doPlayerRemoveItem(cid, 9971, 1)
setPlayerStorageValue(cid,11548,os.time()+0)
doRemoveCreature(cid)
db.executeQuery("UPDATE `player_skills` SET `value` = " .. (sword + 1) .. ", `count` = 0 WHERE `skillid` = 2 and `player_id` = " .. pid .. ";")
return TRUE
else
doPlayerSendTextMessage(cid, MESSAGE_STATUS_CONSOLE_BLUE, "Voc� n�o possui a quantidade necess�ria para comprar.")
return TRUE
end
end

-------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
if(param == "skillaxe") then
if getPlayerSkillLevel(cid, SKILL_AXE) >= 350 then
doPlayerSendTextMessage(cid, MESSAGE_STATUS_CONSOLE_BLUE, "Voc� n�o pode ter skill acima de 350.")
return TRUE
end
if not isKnight(cid) then
doPlayerSendTextMessage(cid, MESSAGE_STATUS_CONSOLE_BLUE, "Somente Knights podem comprar skill de axe.")
return TRUE
end
if getPlayerItemCount(cid, 9971) >= 1 then
local pid = getPlayerGUID(cid)
local axe = getPlayerSkillLevel(cid, SKILL_AXE) 
doPlayerRemoveItem(cid, 9971, 1)
setPlayerStorageValue(cid,11548,os.time()+0)
doRemoveCreature(cid)
db.executeQuery("UPDATE `player_skills` SET `value` = " .. (axe + 1) .. ", `count` = 0 WHERE `skillid` = 3 and `player_id` = " .. pid .. ";")
return TRUE
else
doPlayerSendTextMessage(cid, MESSAGE_STATUS_CONSOLE_BLUE, "Voc� n�o possui a quantidade necess�ria para comprar.")
return TRUE
end
end

-------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
if(param == "skilldistance") then
if getPlayerSkillLevel(cid, SKILL_DISTANCE) >= 350 then
doPlayerSendTextMessage(cid, MESSAGE_STATUS_CONSOLE_BLUE, "Voc� n�o pode ter skill acima de 350.")
return TRUE
end
if not isPaladin(cid) then
doPlayerSendTextMessage(cid, MESSAGE_STATUS_CONSOLE_BLUE, "Somente Paladins podem comprar skill de distance.")
return TRUE
end
if getPlayerItemCount(cid, 9971) >= 1 then
local pid = getPlayerGUID(cid)
local distance = getPlayerSkillLevel(cid, SKILL_DISTANCE) 
doPlayerRemoveItem(cid, 9971, 1)
setPlayerStorageValue(cid,11548,os.time()+0)
doRemoveCreature(cid)
db.executeQuery("UPDATE `player_skills` SET `value` = " .. (distance + 1) .. ", `count` = 0 WHERE `skillid` = 4 and `player_id` = " .. pid .. ";")
return TRUE
else
doPlayerSendTextMessage(cid, MESSAGE_STATUS_CONSOLE_BLUE, "Voc� n�o possui a quantidade necess�ria para comprar.")
return TRUE
end
end

-------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
if(param == "skillshielding") then
if getPlayerSkillLevel(cid, SKILL_SHIELD) >= 350 then
doPlayerSendTextMessage(cid, MESSAGE_STATUS_CONSOLE_BLUE, "Voc� n�o pode ter skill acima de 350.")
return TRUE
end
if(not isPaladin(cid) and not isKnight(cid) and not isDrunou(cid)) then
doPlayerSendTextMessage(cid, MESSAGE_STATUS_CONSOLE_BLUE, "Somente Paladins, Knights e Drunous podem comprar skill de shield.")
return TRUE
end
if getPlayerItemCount(cid, 9971) >= 1 then
local pid = getPlayerGUID(cid)
local shield = getPlayerSkillLevel(cid, SKILL_SHIELD) 
doPlayerRemoveItem(cid, 9971, 1)
setPlayerStorageValue(cid,11548,os.time()+0)
doRemoveCreature(cid)
db.executeQuery("UPDATE `player_skills` SET `value` = " .. (shield + 1) .. ", `count` = 0 WHERE `skillid` = 5 and `player_id` = " .. pid .. ";")
return TRUE
else
doPlayerSendTextMessage(cid, MESSAGE_STATUS_CONSOLE_BLUE, "Voc� n�o possui a quantidade necess�ria para comprar.")
return TRUE
end
end
-------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
if(param == "magiclevel5") then
if getPlayerMagLevel(cid) >= 196 then
doPlayerSendTextMessage(cid, MESSAGE_STATUS_CONSOLE_BLUE, "Voc� n�o pode ter magic level acima de 200.")
return TRUE
end
if(not isSorcerer(cid) and not isDruid(cid) and not isInfernalist(cid)) then
doPlayerSendTextMessage(cid, MESSAGE_STATUS_CONSOLE_BLUE, "Somente Sorcerers, Druids e Infernalists podem comprar magic level.")
return TRUE
end
if getPlayerItemCount(cid, 9971) >= 15 then
local pid = getPlayerGUID(cid) 
doPlayerRemoveItem(cid, 9971, 15)
setPlayerStorageValue(cid,11548,os.time()+0)
doRemoveCreature(cid)
db.executeQuery("UPDATE `players` SET `maglevel` = `maglevel` + 5 WHERE `id` = "..pid)
return TRUE
else
doPlayerSendTextMessage(cid, MESSAGE_STATUS_CONSOLE_BLUE, "Voc� n�o possui a quantidade necess�ria para comprar.")
return TRUE
end
end

-------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
if(param == "skillclub10") then
if getPlayerSkillLevel(cid, SKILL_CLUB) >= 341 then
doPlayerSendTextMessage(cid, MESSAGE_STATUS_CONSOLE_BLUE, "Voc� n�o pode ter skill acima de 350.")
return TRUE
end
if not isDrunou(cid) then
doPlayerSendTextMessage(cid, MESSAGE_STATUS_CONSOLE_BLUE, "Somente Drunous podem comprar skill de club.")
return TRUE
end
if getPlayerItemCount(cid, 9971) >= 10 then
local pid = getPlayerGUID(cid)
local club = getPlayerSkillLevel(cid, SKILL_CLUB) 
doPlayerRemoveItem(cid, 9971, 10)
setPlayerStorageValue(cid,11548,os.time()+0)
doRemoveCreature(cid)
db.executeQuery("UPDATE `player_skills` SET `value` = " .. (club + 10) .. ", `count` = 0 WHERE `skillid` = 1 and `player_id` = " .. pid .. ";")
return TRUE
else
doPlayerSendTextMessage(cid, MESSAGE_STATUS_CONSOLE_BLUE, "Voc� n�o possui a quantidade necess�ria para comprar.")
return TRUE
end
end

-------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
if(param == "skillsword10") then
if getPlayerSkillLevel(cid, SKILL_SWORD) >= 341 then
doPlayerSendTextMessage(cid, MESSAGE_STATUS_CONSOLE_BLUE, "Voc� n�o pode ter skill acima de 350.")
return TRUE
end
if not isKnight(cid) then
doPlayerSendTextMessage(cid, MESSAGE_STATUS_CONSOLE_BLUE, "Somente Knights podem comprar skill de sword.")
return TRUE
end
if getPlayerItemCount(cid, 9971) >= 10 then
local pid = getPlayerGUID(cid)
local sword = getPlayerSkillLevel(cid, SKILL_SWORD) 
doPlayerRemoveItem(cid, 9971, 10)
setPlayerStorageValue(cid,11548,os.time()+0)
doRemoveCreature(cid)
db.executeQuery("UPDATE `player_skills` SET `value` = " .. (sword + 10) .. ", `count` = 0 WHERE `skillid` = 2 and `player_id` = " .. pid .. ";")
return TRUE
else
doPlayerSendTextMessage(cid, MESSAGE_STATUS_CONSOLE_BLUE, "Voc� n�o possui a quantidade necess�ria para comprar.")
return TRUE
end
end

-------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
if(param == "skillaxe10") then
if getPlayerSkillLevel(cid, SKILL_AXE) >= 341 then
doPlayerSendTextMessage(cid, MESSAGE_STATUS_CONSOLE_BLUE, "Voc� n�o pode ter skill acima de 350.")
return TRUE
end
if not isKnight(cid) then
doPlayerSendTextMessage(cid, MESSAGE_STATUS_CONSOLE_BLUE, "Somente Knights podem comprar skill de axe.")
return TRUE
end
if getPlayerItemCount(cid, 9971) >= 10 then
local pid = getPlayerGUID(cid)
local axe = getPlayerSkillLevel(cid, SKILL_AXE) 
doPlayerRemoveItem(cid, 9971, 10)
setPlayerStorageValue(cid,11548,os.time()+0)
doRemoveCreature(cid)
db.executeQuery("UPDATE `player_skills` SET `value` = " .. (axe + 10) .. ", `count` = 0 WHERE `skillid` = 3 and `player_id` = " .. pid .. ";")
return TRUE
else
doPlayerSendTextMessage(cid, MESSAGE_STATUS_CONSOLE_BLUE, "Voc� n�o possui a quantidade necess�ria para comprar.")
return TRUE
end
end

-------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
if(param == "skilldistance10") then
if getPlayerSkillLevel(cid, SKILL_DISTANCE) >= 341 then
doPlayerSendTextMessage(cid, MESSAGE_STATUS_CONSOLE_BLUE, "Voc� n�o pode ter skill acima de 350.")
return TRUE
end
if not isPaladin(cid) then
doPlayerSendTextMessage(cid, MESSAGE_STATUS_CONSOLE_BLUE, "Somente Paladins podem comprar skill de distance.")
return TRUE
end
if getPlayerItemCount(cid, 9971) >= 10 then
local pid = getPlayerGUID(cid)
local distance = getPlayerSkillLevel(cid, SKILL_DISTANCE) 
doPlayerRemoveItem(cid, 9971, 10)
setPlayerStorageValue(cid,11548,os.time()+0)
doRemoveCreature(cid)
db.executeQuery("UPDATE `player_skills` SET `value` = " .. (distance + 10) .. ", `count` = 0 WHERE `skillid` = 4 and `player_id` = " .. pid .. ";")
return TRUE
else
doPlayerSendTextMessage(cid, MESSAGE_STATUS_CONSOLE_BLUE, "Voc� n�o possui a quantidade necess�ria para comprar.")
return TRUE
end
end

-------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
if(param == "skillshielding10") then
if getPlayerSkillLevel(cid, SKILL_SHIELD) >= 341 then
doPlayerSendTextMessage(cid, MESSAGE_STATUS_CONSOLE_BLUE, "Voc� n�o pode ter skill acima de 350.")
return TRUE
end
if(not isPaladin(cid) and not isKnight(cid) and not isDrunou(cid)) then
doPlayerSendTextMessage(cid, MESSAGE_STATUS_CONSOLE_BLUE, "Somente Paladins, Knights e Drunous podem comprar skill de shield.")
return TRUE
end
if getPlayerItemCount(cid, 9971) >= 10 then
local pid = getPlayerGUID(cid)
local shield = getPlayerSkillLevel(cid, SKILL_SHIELD) 
doPlayerRemoveItem(cid, 9971, 10)
setPlayerStorageValue(cid,11548,os.time()+0)
doRemoveCreature(cid)
db.executeQuery("UPDATE `player_skills` SET `value` = " .. (shield + 10) .. ", `count` = 0 WHERE `skillid` = 5 and `player_id` = " .. pid .. ";")
return TRUE
else
doPlayerSendTextMessage(cid, MESSAGE_STATUS_CONSOLE_BLUE, "Voc� n�o possui a quantidade necess�ria para comprar.")
return TRUE
end
end

-------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
if(param == "removerfrag") then
if getPlayerItemCount(cid, 2160) >= 10 then
doPlayerRemoveItem(cid, 2160, 10)
db.executeQuery("UPDATE `killers` SET `unjustified` = 0 WHERE `unjustified` = 1 AND `id` IN (SELECT `kill_id` FROM `player_killers` WHERE `player_id` = " .. getPlayerGUID(cid) .. ") LIMIT 1;")
setPlayerStorageValue(cid,11548,os.time()+30)
doPlayerSendTextMessage(cid, MESSAGE_STATUS_CONSOLE_BLUE, "Parab�ns voc� removeu seus frags com sucesso.")
return TRUE
else
doPlayerSendTextMessage(cid, MESSAGE_STATUS_CONSOLE_BLUE, "Voc� n�o possui a quantidade necess�ria para comprar.")
return TRUE
end
end

-------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
if(param == "novark") then
if getPlayerItemCount(cid, 9971) >= 10 then
setPlayerRecoveryKey(cid)
doPlayerRemoveItem(cid, 9971, 10)
doPlayerSendTextMessage(cid, MESSAGE_STATUS_CONSOLE_BLUE, "ATEN��O! Esta ser� a �nica vez que sua RK ser� mostrada, portanto, anote-a em um lugar seguro fora de seu computador!\n" .. setPlayerRecoveryKey(cid) .. "")
setPlayerStorageValue(cid,11548,os.time()+30)
return TRUE
else
doPlayerSendTextMessage(cid, MESSAGE_STATUS_CONSOLE_BLUE, "Voc� n�o possui a quantidade necess�ria para comprar.")
return TRUE
end
end
-------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
if(param == "stamina up") then
if getPlayerItemCount(cid, 9971) >= 100 then
local bag = doPlayerAddItem(cid, 1997, 1)
local new_item = doAddContainerItem(bag, 12505, 1)
doItemSetAttribute(new_item, "description", "Este item pode ser adquirido atrav�s do shopping. Adquirido dia " .. os.date("%d/%m/%Y - %X") .." por ".. getPlayerName(cid) ..". Serial: "..  getPlayerGUID(cid) ..".")
doPlayerRemoveItem(cid, 9971, 60)
doPlayerSendTextMessage(cid, MESSAGE_STATUS_CONSOLE_BLUE, "Voc� comprou uma Stamina UP com sucesso.")
setPlayerStorageValue(cid,11548,os.time()+30)
return TRUE
else
doPlayerSendTextMessage(cid, MESSAGE_STATUS_CONSOLE_BLUE, "Voc� n�o possui a quantidade necess�ria para comprar.")
end
end
return TRUE
end