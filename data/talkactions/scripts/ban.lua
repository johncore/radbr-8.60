function onSay(cid, words, param, channel)
if param == '' then
return true
end
local params = string.explode(param, ',')
if #params > 3 then
local player = getPlayerByName(params[1])
if player then
params[2] = tonumber(params[2])
params[4] = tonumber(params[4])
doAddAccountBanishment(getPlayerAccountId(player), player, os.time() + params[2] * 24 * 60 * 60, params[4], ACTION_BANISHMENT, params[3], cid)
db.executeQuery("UPDATE `accounts` SET `warnings` = `warnings` + 1 WHERE `id` = " .. getPlayerAccountId(player) .. ";")
doSendMagicEffect(getCreaturePosition(player), CONST_ME_MAGIC_RED)
doSendAnimatedText(getCreaturePosition(player), "BANIDO", TEXTCOLOR_RED)
doPlayerSendTextMessage(cid, MESSAGE_STATUS_WARNING, "Jogador " .. getCreatureName(player) .. " foi banido.")
doRemoveCreature(player)
end
else
doPlayerSendTextMessage(cid, MESSAGE_STATUS_CONSOLE_BLUE, 'Parâmetros Incorreto.\nComo Usar: /ban {Nome},{Dias},{Comentário},{Motivo}.')
end
return true
end 