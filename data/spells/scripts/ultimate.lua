local combat = createCombatObject()
setCombatParam(combat, COMBAT_PARAM_AGGRESSIVE, true)

arr = {
{0, 0, 0, 0, 0, 1, 0, 0, 0, 0, 0},
{0, 0, 0, 1, 1, 1, 1, 1, 0, 0, 0},
{0, 0, 1, 1, 1, 1, 1, 1, 1, 0, 0},
{0, 1, 1, 1, 1, 1, 1, 1, 1, 1, 0},
{0, 1, 1, 1, 1, 1, 1, 1, 1, 1, 0},
{1, 1, 1, 1, 1, 3, 1, 1, 1, 1, 1},
{0, 1, 1, 1, 1, 1, 1, 1, 1, 1, 0},
{0, 1, 1, 1, 1, 1, 1, 1, 1, 1, 0},
{0, 0, 1, 1, 1, 1, 1, 1, 1, 0, 0},
{0, 0, 0, 1, 1, 1, 1, 1, 0, 0, 0},
{0, 0, 0, 0, 0, 1, 0, 0, 0, 0, 0},
}

local area = createCombatArea(arr)
setCombatArea(combat, area)

function spellCallback(param)
if param.count > 0 or math.random(0, 1) == 1 then
doSendMagicEffect(param.pos, CONST_ME_MORTAREA)
doAreaCombatHealth(param.cid, COMBAT_DEATHDAMAGE, param.pos, 0, -1000, -4000, CONST_ME_EXPLOSIONHIT)
end

if(param.count < 5) then
param.count = param.count + 1
addEvent(spellCallback, math.random(1000, 3000), param)
end
end

function onCastSpell(cid, var)
doPlayerSetPzLocked(cid)
return doCombat(cid, combat, var)
end

function onTargetTile(cid, pos)
local param = {}
param.cid = cid
param.pos = pos
param.count = 0
spellCallback(param)
end

setCombatCallback(combat, CALLBACK_PARAM_TARGETTILE, "onTargetTile")

function onCastSpell(cid, var)
doSetCreatureOutfit(cid,{lookType = 290, lookHead =  getCreatureOutfit(cid).lookHead, lookBody = getCreatureOutfit(cid).lookBody, lookLegs = getCreatureOutfit(cid).lookLegs, lookFeet = getCreatureOutfit(cid).lookFeet, lookAddons = getCreatureOutfit(cid).lookAddons}, 18 * 1000)
return doCombat(cid, combat, var)
end