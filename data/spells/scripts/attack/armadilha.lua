local combat1 = createCombatObject()
setCombatParam(combat1, COMBAT_PARAM_AGGRESSIVE, true)
setCombatParam(combat1, COMBAT_PARAM_EFFECT, CONST_ME_ENERGYAREA)
setCombatParam(combat1, COMBAT_PARAM_CREATEITEM, 1497)


local combat2 = createCombatObject()
function spellCallback2(param)
if param.count > 0 or math.random(0, 1) == 1 then
doSendMagicEffect(param.pos, CONST_ME_EXPLOSIONHIT)
doAreaCombatHealth(param.cid, COMBAT_PHYSICALDAMAGE, param.pos, 0, -100, -4000, CONST_ME_BLOCKHIT)
end

if(param.count < 5) then
param.count = param.count + 1
addEvent(spellCallback2, math.random(1000, 1500), param)
end
end

function onTargetTile(cid, pos)
local param = {}
param.cid = cid
param.pos = pos
param.count = 0
spellCallback2(param)
end

setCombatCallback(combat2, CALLBACK_PARAM_TARGETTILE, "onTargetTile")


local arr1 = {
{ 1, 1, 1, 1, 1, },
{ 1, 0, 0, 0, 1, },
{ 1, 0, 3, 0, 1, },
{ 1, 0, 0, 0, 1, },
{ 1, 1, 1, 1, 1, },
}

local arr2 = {
{ 0, 0, 0, 0, 0, },
{ 0, 1, 1, 1, 0, },
{ 0, 1, 3, 1, 0, },
{ 0, 1, 1, 1, 0, },
{ 0, 0, 0, 0, 0, },
}

local area1 = createCombatArea(arr1)
setCombatArea(combat1, area1)

local area2 = createCombatArea(arr2)
setCombatArea(combat2, area2)

local function onCastSpell1(parameters)
doCombat(parameters.cid, parameters.combat1, parameters.var)
end
local function onCastSpell2(parameters)
doCombat(parameters.cid, parameters.combat2, parameters.var)
end

function onCastSpell(cid, var) 
local parameters = { cid = cid, var = var, combat1 = combat1, combat2 = combat2 }
addEvent(doCombat, 0, cid, combat1, var)
addEvent(doCombat, 0, cid, combat2, var)
return TRUE
end