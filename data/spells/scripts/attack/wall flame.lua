local combat = createCombatObject()
setCombatParam(combat, COMBAT_PARAM_TYPE, COMBAT_FIREDAMAGE)
setCombatParam(combat, COMBAT_PARAM_EFFECT, CONST_ME_FIREATTACK)
setCombatParam(combat, COMBAT_PARAM_DISTANCEEFFECT, CONST_ANI_FIRE)
setCombatParam(combat, COMBAT_PARAM_CREATEITEM, 1492)
function onGetFormulaValues(cid, level, maglevel)
min = -(level * 2 + maglevel * 3) * 0.8
max = -(level * 2 + maglevel * 3) * 0.9
	return min, max
end

setCombatCallback(combat, CALLBACK_PARAM_LEVELMAGICVALUE, "onGetFormulaValues")


local arr = {
{0, 0, 0, 0, 0, 0 ,0},
{0, 0, 0, 0, 0, 0 ,0},
{0, 0, 0, 0, 0, 0 ,0},
{0, 0, 0, 1, 0, 0 ,0},
{0, 0, 0, 1, 0, 0 ,0},
{0, 0, 0, 1, 0, 0 ,0},
{0, 0, 0, 1, 0, 0 ,0},
{0, 0, 0, 1, 0, 0 ,0},
{0, 0, 0, 1, 0, 0 ,0},
{0, 0, 0, 3, 0, 0 ,0},
{0, 0, 0, 0, 0, 0 ,0},
{0, 0, 0, 0, 0, 0 ,0},
{0, 0, 0, 0, 0, 0 ,0}
}

local area = createCombatArea(arr)
setCombatArea(combat, area)

function onCastSpell(cid, var)
	return doCombat(cid, combat, var)
end