local combat1 = createCombatObject()
setCombatParam(combat1, COMBAT_PARAM_TYPE, COMBAT_HOLYDAMAGE)
setCombatParam(combat1, COMBAT_PARAM_EFFECT, 49)
setCombatParam(combat1, COMBAT_PARAM_USECHARGES, TRUE)
function onGetFormulaValues(cid, level, maglevel)
min = -(level * 2 + maglevel * 4) * 4.5
max = -(level * 2 + maglevel * 4) * 4.9
	return min, max
end
setCombatCallback(combat1, CALLBACK_PARAM_LEVELMAGICVALUE, "onGetFormulaValues")

local combat2 = createCombatObject()
setCombatParam(combat2, COMBAT_PARAM_TYPE, COMBAT_HEALING)
setCombatParam(combat2, COMBAT_PARAM_EFFECT, CONST_ME_MAGIC_BLUE)
setCombatParam(combat2, COMBAT_PARAM_AGGRESSIVE, FALSE)
setCombatParam(combat2, COMBAT_PARAM_DISPEL, CONDITION_PARALYZE)
function onGetFormulaValues(cid, level, maglevel)
min = (level * 2 + maglevel * 4) * 1.8
max = (level * 2 + maglevel * 4) * 2.0
if min < 250 then
min = 250
end
return min, max
end
setCombatCallback(combat2, CALLBACK_PARAM_LEVELMAGICVALUE, "onGetFormulaValues")

local arr1 = {
{0, 0, 0, 0, 0, 0 ,0},
{0, 0, 0, 0, 0, 0 ,0},
{0, 0, 1, 1, 1, 0 ,0},
{0, 0, 1, 2, 1, 0 ,0},
{0, 0, 1, 1, 1, 0 ,0},
{0, 0, 0, 0, 0, 0 ,0},
{0, 0, 0, 0, 0, 0 ,0}
}

local area1 = createCombatArea(arr1)
setCombatArea(combat1, area1)

local function onCastSpell1(parameters)
doCombat(parameters.cid, parameters.combat1, parameters.var)
end
local function onCastSpell2(parameters)
doCombat(parameters.cid, parameters.combat2, parameters.var)
end

function onCastSpell(cid, var) 
local parameters = { cid = cid, var = var, combat1 = combat1, combat2 = combat2 }
addEvent(onCastSpell1, 100, parameters) 
addEvent(onCastSpell2, 200, parameters) 
return TRUE
end