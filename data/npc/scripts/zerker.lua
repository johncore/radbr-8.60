local keywordHandler = KeywordHandler:new()
local npcHandler = NpcHandler:new(keywordHandler)
NpcSystem.parseParameters(npcHandler)
local talkState = {}

local pos = {x=1072, y=1429, z=7} -------------- Pos para onde o player sera levado
local itemid = 7290 ----------------- Id do item que vai ser removido do player

function onCreatureAppear(cid)            npcHandler:onCreatureAppear(cid)        end
function onCreatureDisappear(cid)        npcHandler:onCreatureDisappear(cid)        end
function onCreatureSay(cid, type, msg)        npcHandler:onCreatureSay(cid, type, msg)    end
function onThink()                npcHandler:onThink()                end

function creatureSayCallback(cid, type, msg)

    if(not npcHandler:isFocused(cid)) then
        return false
    end

    if msgcontains(msg, "ilhas perdidas") then
        selfSay("Basta agora voc� me dar um {"..getItemNameById(itemid).."} e mais 750 gps que eu te levarei.", cid)
        talkState[cid] = 0
    elseif msgcontains(msg, 'shard') then
       if doPlayerRemoveItem(cid, itemid, 1) then
     doTeleportThing(cid, pos)
     else
     selfSay("� necess�rio um {"..getItemNameById(itemid).."} pra que eu possa fazer o feiti�o de teleporte.", cid)
     end
     end
     return TRUE   
     end
     

npcHandler:setCallback(CALLBACK_MESSAGE_DEFAULT, creatureSayCallback)
npcHandler:addModule(FocusModule:new())