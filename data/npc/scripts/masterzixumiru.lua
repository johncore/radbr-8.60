local keywordHandler = KeywordHandler:new()
local npcHandler = NpcHandler:new(keywordHandler)
NpcSystem.parseParameters(npcHandler)
local talkState = {}

function onCreatureAppear(cid)				npcHandler:onCreatureAppear(cid)			end
function onCreatureDisappear(cid) 			npcHandler:onCreatureDisappear(cid)			end
function onCreatureSay(cid, type, msg)			npcHandler:onCreatureSay(cid, type, msg)		end
function onThink()					npcHandler:onThink()					end

local shopModule = ShopModule:new()
npcHandler:addModule(shopModule)

shopModule:addBuyableItem({'blank rune'}, 2260, 15, 1, 'blank rune')
shopModule:addBuyableItem({'destroy field'}, 2261, 5, 1, 'destroy field rune')
shopModule:addBuyableItem({'energy bomb'}, 2262, 80, 1, 'energy bomb rune')
shopModule:addBuyableItem({'explosion'}, 2313, 15, 1, 'explosion rune')
shopModule:addBuyableItem({'firebomb'}, 2305, 58, 1, 'firebomb rune')
shopModule:addBuyableItem({'great fireball'}, 2304, 120, 1, 'great fireball rune')
shopModule:addBuyableItem({'hailstorm rod', 'hailstorm'}, 2183, 15000, 1, 'hailstorm rod')
shopModule:addBuyableItem({'heavy magic missile'}, 2311, 6, 1, 'heavy magic missile rune')
shopModule:addBuyableItem({'livro n�vel 2'}, 8901, 8000, 1, 'livro n�vel 2')
shopModule:addBuyableItem({'livro n�vel 3'}, 8900, 15000, 1, 'livro n�vel 3')
shopModule:addBuyableItem({'magic wall'}, 2293, 58, 1, 'magic wall rune')
shopModule:addBuyableItem({'moonlight rod', 'moonlight'}, 2186, 1000, 1, 'moonlight rod')
shopModule:addBuyableItem({'paralyze'}, 2278, 350, 1, 'paralyze rune')
shopModule:addBuyableItem({'poison bomb'}, 2286, 42, 1, 'poison bomb rune')
shopModule:addBuyableItem({'snakebite rod', 'snakebite'}, 2182, 500, 1, 'snakebite rod')
shopModule:addBuyableItem({'sudden death'}, 2268, 54, 1, 'sudden death rune')
shopModule:addBuyableItem({'terra rod', 'terra'}, 2181, 10000, 1, 'terra rod')
shopModule:addBuyableItem({'ultimate healing'}, 2273, 87, 1, 'ultimate healing rune')
shopModule:addBuyableItem({'wand of cosmic energy', 'cosmic energy'}, 2189, 10000, 1, 'wand of cosmic energy')
shopModule:addBuyableItem({'wand of decay', 'decay'}, 2188, 5000, 1, 'wand of decay')
shopModule:addBuyableItem({'wand of dragonbreath', 'dragonbreath'}, 2191, 1000, 1, 'wand of dragonbreath')
shopModule:addBuyableItem({'wand of inferno', 'inferno'}, 2187, 15000, 1, 'wand of inferno')
shopModule:addBuyableItem({'wand of vortex', 'vortex'}, 2190, 500, 1, 'wand of vortex')
shopModule:addBuyableItem({'frasco de mana m�dio vazio', 'm�dio'}, 7636, 50, 'frasco de mana m�dio vazio')
shopModule:addBuyableItem({'frasco de mana grande vazio', 'grande'}, 7634, 100, 'frasco de mana grande vazio')
shopModule:addBuyableItem({'frasco de mana gigante vazio', 'gigante'}, 7635, 200, 'frasco de mana gigante vazio')
shopModule:addBuyableItem({'frasco de mana m�dio'}, 7620, 200, 1, 'frasco de mana m�dio')
shopModule:addBuyableItem({'frasco de mana grande'}, 7589, 500, 1, 'frasco de mana grande')
shopModule:addBuyableItem({'frasco de mana gigante'}, 7590, 1000, 1, 'frasco de mana gigante')

npcHandler:setCallback(CALLBACK_MESSAGE_DEFAULT, creatureSayCallback)
npcHandler:addModule(FocusModule:new())
