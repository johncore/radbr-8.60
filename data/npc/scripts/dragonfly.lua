local keywordHandler = KeywordHandler:new()
local npcHandler = NpcHandler:new(keywordHandler)
NpcSystem.parseParameters(npcHandler)
 
        
        
-- OTServ event handling functions start
function onCreatureAppear(cid)				npcHandler:onCreatureAppear(cid) end
function onCreatureDisappear(cid) 			npcHandler:onCreatureDisappear(cid) end
function onCreatureSay(cid, type, msg) 	npcHandler:onCreatureSay(cid, type, msg) end
function onThink() 						npcHandler:onThink() end
-- OTServ event handling functions end
   

	
-- Don't forget npcHandler = npcHandler in the parameters. It is required for all StdModule functions!
local travelNode = keywordHandler:addKeyword({'dragonworld'}, StdModule.say, {npcHandler = npcHandler, onlyFocus = true, text = 'Voc� deseja ir para DragonWorld em troca de 900 gps?'})
	travelNode:addChildKeyword({'yes'}, StdModule.travel, {npcHandler = npcHandler, premium = true, level = 0, cost = 900, destination = {x=1583, y=1796, z=6} })
	travelNode:addChildKeyword({'sim'}, StdModule.travel, {npcHandler = npcHandler, premium = true, level = 0, cost = 900, destination = {x=1583, y=1796, z=6} })

	-- Makes sure the npc reacts when you say hi, bye etc.
npcHandler:addModule(FocusModule:new())