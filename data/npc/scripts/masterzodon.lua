local keywordHandler = KeywordHandler:new()
local npcHandler = NpcHandler:new(keywordHandler)
NpcSystem.parseParameters(npcHandler)
local talkState = {}

function onCreatureAppear(cid)				npcHandler:onCreatureAppear(cid)			end
function onCreatureDisappear(cid) 			npcHandler:onCreatureDisappear(cid)			end
function onCreatureSay(cid, type, msg)			npcHandler:onCreatureSay(cid, type, msg)		end
function onThink()					npcHandler:onThink()					end

local shopModule = ShopModule:new()
npcHandler:addModule(shopModule)

shopModule:addBuyableItem({'blank rune'}, 2260, 7, 1, 'blank rune')
shopModule:addBuyableItem({'ceremonial ankh'}, 6561, 3700, 1, 'ceremonial ankh')
shopModule:addBuyableItem({'destroy field'}, 2261, 5, 1, 'destroy field rune')
shopModule:addBuyableItem({'energy bomb'}, 2262, 77, 1, 'energy bomb rune')
shopModule:addBuyableItem({'explosion'}, 2313, 15, 1, 'explosion rune')
shopModule:addBuyableItem({'firebomb'}, 2305, 56, 1, 'firebomb rune')
shopModule:addBuyableItem({'frasco de mana gigante'}, 7590, 3600, 1, 'frasco de mana gigante')
shopModule:addBuyableItem({'frasco de mana grande'}, 7589, 1600, 1, 'frasco de mana grande')
shopModule:addBuyableItem({'frasco de mana m�dio'}, 7620, 290, 1, 'frasco de mana m�dio')
shopModule:addBuyableItem({'great fireball'}, 2304, 22, 1, 'great fireball rune')
shopModule:addBuyableItem({'hailstorm rod', 'hailstorm'}, 2183, 14000, 1, 'hailstorm rod')
shopModule:addBuyableItem({'heavy magic missile'}, 2311, 6, 1, 'heavy magic missile rune')
shopModule:addBuyableItem({'livro n�vel 1'}, 8903, 1000, 1, 'livro n�vel 1')
shopModule:addBuyableItem({'livro n�vel 2'}, 8901, 8000, 1, 'livro n�vel 2')
shopModule:addBuyableItem({'livro n�vel 3'}, 8900, 15000, 1, 'livro n�vel 3')
shopModule:addBuyableItem({'magic wall'}, 2293, 55, 1, 'magic wall rune')
shopModule:addBuyableItem({'moonlight rod', 'moonlight'}, 2186, 900, 1, 'moonlight rod')
shopModule:addBuyableItem({'necrotic rod', 'necrotic'}, 2185, 4500, 1, 'necrotic rod')
shopModule:addBuyableItem({'paralyze'}, 2278, 360, 1, 'paralyze rune')
shopModule:addBuyableItem({'poison bomb'}, 2286, 40, 1, 'poison bomb rune')
shopModule:addBuyableItem({'snakebite rod', 'snakebite'}, 2182, 450, 1, 'snakebite rod')
shopModule:addBuyableItem({'sudden death'}, 2268, 52, 1, 'sudden death rune')
shopModule:addBuyableItem({'terra rod', 'terra'}, 2181, 9000, 1,'terra rod')
shopModule:addBuyableItem({'ultimate healing'}, 2273, 84, 1, 'ultimate healing rune')
shopModule:addBuyableItem({'wand of cosmic energy', 'cosmic energy'}, 2189, 9000, 1, 'wand of cosmic energy')
shopModule:addBuyableItem({'wand of decay', 'decay'}, 2188, 4500, 1,'wand of decay')
shopModule:addBuyableItem({'wand of dragonbreath', 'dragonbreath'}, 2191, 900, 1, 'wand of dragonbreath')
shopModule:addBuyableItem({'wand of inferno', 'inferno'}, 2187, 14000, 1, 'wand of inferno')
shopModule:addBuyableItem({'wand of vortex', 'vortex'}, 2190, 450, 1, 'wand of vortex')


npcHandler:setCallback(CALLBACK_MESSAGE_DEFAULT, creatureSayCallback)
npcHandler:addModule(FocusModule:new())
