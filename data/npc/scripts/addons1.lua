local keywordHandler = KeywordHandler:new()
local npcHandler = NpcHandler:new(keywordHandler)
NpcSystem.parseParameters(npcHandler)


function onCreatureAppear(cid)                npcHandler:onCreatureAppear(cid)             end
function onCreatureDisappear(cid)             npcHandler:onCreatureDisappear(cid)         end
function onCreatureSay(cid, type, msg)         npcHandler:onCreatureSay(cid, type, msg)     end
function onThink()                             npcHandler:onThink()                         end

npcHandler:setMessage(MESSAGE_GREET, "Ol� |PLAYERNAME|. Eu vendo todos os primeiros addons, por 500k cada. � s� me falar o nome do addon que voc� quer. Exemplo: knight.")

function playerBuyAddonNPC(cid, message, keywords, parameters, node)
    if(not npcHandler:isFocused(cid)) then
        return false
    end
    if (parameters.confirm ~= true) and (parameters.decline ~= true) then
        if(getPlayerPremiumDays(cid) == 1) and (parameters.premium == true) then
            npcHandler:say('Desculpe, mas este addon � somente para jogadores premium!', cid)
            npcHandler:resetNpc()
            return true
        end
        if (getPlayerStorageValue(cid, parameters.storageID) ~= -1) then
            npcHandler:say('Voc� j� tem este addon!', cid)
            npcHandler:resetNpc()
            return true
        end
        local itemsTable = parameters.items
        local items_list = ''
        if table.maxn(itemsTable) > 0 then
            for i = 1, table.maxn(itemsTable) do
                local item = itemsTable[i]
                items_list = items_list .. item[2] .. ' ' .. getItemNameById(item[1])
                if i ~= table.maxn(itemsTable) then
                    items_list = items_list .. ', '
                end
            end
        end
        local text = ''
        if (parameters.cost > 0) and table.maxn(parameters.items) then
            text = items_list .. ' and ' .. parameters.cost .. ' gp'
        elseif (parameters.cost > 0) then
            text = parameters.cost .. ' gp'
        elseif table.maxn(parameters.items) then
            text = items_list
        end
        npcHandler:say('Voc� trouxe-me ' .. text .. ' para ' .. keywords[1] .. '?', cid)
        return true
    elseif (parameters.confirm == true) then
        local addonNode = node:getParent()
        local addoninfo = addonNode:getParameters()
        local items_number = 0
        if table.maxn(addoninfo.items) > 0 then
            for i = 1, table.maxn(addoninfo.items) do
                local item = addoninfo.items[i]
                if (getPlayerItemCount(cid,item[1]) >= item[2]) then
                    items_number = items_number + 1
                end
            end
        end
        if(getPlayerMoney(cid) >= addoninfo.cost) and (items_number == table.maxn(addoninfo.items)) then
            doPlayerRemoveMoney(cid, addoninfo.cost)
            if table.maxn(addoninfo.items) > 0 then
                for i = 1, table.maxn(addoninfo.items) do
                    local item = addoninfo.items[i]
                    doPlayerRemoveItem(cid,item[1],item[2])
                end
            end
            doPlayerAddOutfit(cid, addoninfo.outfit_male, addoninfo.addon)
            doPlayerAddOutfit(cid, addoninfo.outfit_female, addoninfo.addon)
            setPlayerStorageValue(cid,addoninfo.storageID,1)
            npcHandler:say('Muito obrigado, aqui est� seu addon.', cid)
        else
            npcHandler:say('Voc� n�o tem dinheiro suficiente para este addon!', cid)
        end
        npcHandler:resetNpc()
        return true
    elseif (parameters.decline == true) then
        npcHandler:say('N�o est� interessado neste? Talvez o outro addon?', cid)
        npcHandler:resetNpc()
        return true
    end
    return false
end

local noNode = KeywordNode:new({'no'}, playerBuyAddonNPC, {decline = true})
local yesNode = KeywordNode:new({'yes'}, playerBuyAddonNPC, {confirm = true})

-- citizen (done)
local outfit_node = keywordHandler:addKeyword({'citizen'}, playerBuyAddonNPC, {premium = false, cost = 0, items = {{2160,50}}, outfit_female = 136, outfit_male = 128, addon = 1, storageID = 10001})
outfit_node:addChildKeywordNode(yesNode)
outfit_node:addChildKeywordNode(noNode)

-- hunter (done)
local outfit_node = keywordHandler:addKeyword({'hunter'}, playerBuyAddonNPC, {premium = false, cost = 0, items = {{2160,50}}, outfit_female = 137, outfit_male = 129, addon = 1, storageID = 10003})
outfit_node:addChildKeywordNode(yesNode)
outfit_node:addChildKeywordNode(noNode)

-- knight (done)
local outfit_node = keywordHandler:addKeyword({'knight'}, playerBuyAddonNPC, {premium = false, cost = 0, items = {{2160,50}}, outfit_female = 139, outfit_male = 131, addon = 1, storageID = 10005})
outfit_node:addChildKeywordNode(yesNode)
outfit_node:addChildKeywordNode(noNode)

-- mage (done)
local outfit_node = keywordHandler:addKeyword({'mage'}, playerBuyAddonNPC, {premium = false, cost = 0, items = {{2160,50}}, outfit_female = 138, outfit_male = 130, addon = 1, storageID = 10007})
outfit_node:addChildKeywordNode(yesNode)
outfit_node:addChildKeywordNode(noNode)


-- summoner (done)
local outfit_node = keywordHandler:addKeyword({'summoner'}, playerBuyAddonNPC, {premium = false, cost = 0, items = {{2160,50}}, outfit_female = 141, outfit_male = 133, addon = 1, storageID = 10009})
outfit_node:addChildKeywordNode(yesNode)
outfit_node:addChildKeywordNode(noNode)

-- barbarian (done)
local outfit_node = keywordHandler:addKeyword({'barbarian'}, playerBuyAddonNPC, {premium = false, cost = 0, items = {{2160,50}}, outfit_female = 147, outfit_male = 143, addon = 1, storageID = 10011})
outfit_node:addChildKeywordNode(yesNode)
outfit_node:addChildKeywordNode(noNode)

-- druid (done)
local outfit_node = keywordHandler:addKeyword({'druid'}, playerBuyAddonNPC, {premium = false, cost = 0, items = {{2160,50}}, outfit_female = 148, outfit_male = 144, addon = 1, storageID = 10013})
outfit_node:addChildKeywordNode(yesNode)
outfit_node:addChildKeywordNode(noNode)

-- nobleman (done)
local outfit_node = keywordHandler:addKeyword({'nobleman'}, playerBuyAddonNPC, {premium = false, cost = 0, items = {{2160,50}}, outfit_female = 140, outfit_male = 132, addon = 1, storageID = 10015})
outfit_node:addChildKeywordNode(yesNode)
outfit_node:addChildKeywordNode(noNode)

-- oriental (done)
local outfit_node = keywordHandler:addKeyword({'oriental'}, playerBuyAddonNPC, {premium = false, cost = 0, items = {{2160,50}}, outfit_female = 150, outfit_male = 146, addon = 1, storageID = 10017})
outfit_node:addChildKeywordNode(yesNode)
outfit_node:addChildKeywordNode(noNode)

-- warrior (done)
local outfit_node = keywordHandler:addKeyword({'warrior'}, playerBuyAddonNPC, {premium = false, cost = 0, items = {{2160,50}}, outfit_female = 142, outfit_male = 134, addon = 1, storageID = 10019})
outfit_node:addChildKeywordNode(yesNode)
outfit_node:addChildKeywordNode(noNode)

-- wizard (done)
local outfit_node = keywordHandler:addKeyword({'wizard'}, playerBuyAddonNPC, {premium = false, cost = 0, items = {{2160,50}}, outfit_female = 149, outfit_male = 145, addon = 1, storageID = 10021})
outfit_node:addChildKeywordNode(yesNode)
outfit_node:addChildKeywordNode(noNode)

-- assassin (done)
local outfit_node = keywordHandler:addKeyword({'assassin'}, playerBuyAddonNPC, {premium = false, cost = 0, items = {{2160,50}}, outfit_female = 156, outfit_male = 152, addon = 1, storageID = 10023})
outfit_node:addChildKeywordNode(yesNode)
outfit_node:addChildKeywordNode(noNode)

-- beggar (done)
local outfit_node = keywordHandler:addKeyword({'beggar'}, playerBuyAddonNPC, {premium = false, cost = 0, items = {{2160,50}}, outfit_female = 157, outfit_male = 153, addon = 1, storageID = 10025})
outfit_node:addChildKeywordNode(yesNode)
outfit_node:addChildKeywordNode(noNode)

-- pirate (done)
local outfit_node = keywordHandler:addKeyword({'pirate'}, playerBuyAddonNPC, {premium = false, cost = 0, items = {{2160,50}}, outfit_female = 155, outfit_male = 151, addon = 1, storageID = 10027})
outfit_node:addChildKeywordNode(yesNode)
outfit_node:addChildKeywordNode(noNode)

-- shaman (done)
local outfit_node = keywordHandler:addKeyword({'shaman'}, playerBuyAddonNPC, {premium = false, cost = 0, items = {{2160,50}}, outfit_female = 158, outfit_male = 154, addon = 1, storageID = 10029})
outfit_node:addChildKeywordNode(yesNode)
outfit_node:addChildKeywordNode(noNode)

-- norseman (done)
local outfit_node = keywordHandler:addKeyword({'norseman'}, playerBuyAddonNPC, {premium = false, cost = 0, items = {{2160,50}}, outfit_female = 252, outfit_male = 251, addon = 1, storageID = 10031})
outfit_node:addChildKeywordNode(yesNode)
outfit_node:addChildKeywordNode(noNode)

-- jester (done)
local outfit_node = keywordHandler:addKeyword({'jester'}, playerBuyAddonNPC, {premium = false, cost = 0, items = {{2160,50}}, outfit_female = 270, outfit_male = 273, addon = 1, storageID = 10033})
outfit_node:addChildKeywordNode(yesNode)
outfit_node:addChildKeywordNode(noNode)

-- demonhunter (done)
local outfit_node = keywordHandler:addKeyword({'demon'}, playerBuyAddonNPC, {premium = false, cost = 0, items = {{2160,50}}, outfit_female = 288, outfit_male = 289, addon = 1, storageID = 10035})
outfit_node:addChildKeywordNode(yesNode)
outfit_node:addChildKeywordNode(noNode)

-- nightmare (done)
local outfit_node = keywordHandler:addKeyword({'nightmare'}, playerBuyAddonNPC, {premium = false, cost = 0, items = {{2160,50}}, outfit_female = 269, outfit_male = 268, addon = 1, storageID = 10037})
outfit_node:addChildKeywordNode(yesNode)
outfit_node:addChildKeywordNode(noNode)

-- brotherhood (done)
local outfit_node = keywordHandler:addKeyword({'brotherhood'}, playerBuyAddonNPC, {premium = false, cost = 0, items = {{2160,50}}, outfit_female = 136, outfit_male = 278, addon = 1, storageID = 10039})
outfit_node:addChildKeywordNode(yesNode)
outfit_node:addChildKeywordNode(noNode)

-- yalaharian (done)
local outfit_node = keywordHandler:addKeyword({'yalaharian'}, playerBuyAddonNPC, {premium = false, cost = 0, items = {{2160,50}}, outfit_female = 324, outfit_male = 325, addon = 1, storageID = 10041})
outfit_node:addChildKeywordNode(yesNode)
outfit_node:addChildKeywordNode(noNode)

-- warmaster (done)
local outfit_node = keywordHandler:addKeyword({'warmaster'}, playerBuyAddonNPC, {premium = false, cost = 0, items = {{2160,50}}, outfit_female = 336, outfit_male = 337, addon = 1, storageID = 10043})
outfit_node:addChildKeywordNode(yesNode)
outfit_node:addChildKeywordNode(noNode)

-- wayfarer (done)
local outfit_node = keywordHandler:addKeyword({'wayfarer'}, playerBuyAddonNPC, {premium = false, cost = 0, items = {{2160,50}}, outfit_female = 366, outfit_male = 366, addon = 1, storageID = 10045})
outfit_node:addChildKeywordNode(yesNode)
outfit_node:addChildKeywordNode(noNode)

keywordHandler:addKeyword({'addons'}, StdModule.say, {npcHandler = npcHandler, onlyFocus = true, text = 'Posso dar-lhe citizen, hunter, knight, mage, nobleman, summoner, warrior, barbarian, druid, wizard, oriental, pirate, assassin, beggar, shaman, norseman, nighmare, jester, brotherhood, yalaharian, warmaster e wayfarer addons.'})
keywordHandler:addKeyword({'help'}, StdModule.say, {npcHandler = npcHandler, onlyFocus = true, text = 'Para comprar o primeiro addon fale {NAME addon}, para o segundo fale {second NAME addon}.'})

npcHandler:addModule(FocusModule:new())
