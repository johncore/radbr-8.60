local keywordHandler = KeywordHandler:new()
local npcHandler = NpcHandler:new(keywordHandler)
local target = -3
NpcSystem.parseParameters(npcHandler)



-- OTServ event handling functions start
function onCreatureAppear(cid)				npcHandler:onCreatureAppear(cid) end
function onCreatureDisappear(cid) 			npcHandler:onCreatureDisappear(cid) end
function onCreatureSay(cid, type, msg) 	npcHandler:onCreatureSay(cid, type, msg) end
function onThink() 						npcHandler:onThink() end
-- OTServ event handling functions end

-- Don't forget npcHandler = npcHandler in the parameters. It is required for all StdModule functions!
local travelNode = keywordHandler:addKeyword({'wisland'}, StdModule.say, {npcHandler = npcHandler, onlyFocus = true, text = 'Voc� deseja ir para Wisland em troca de 100 gps?'})
	travelNode:addChildKeyword({'yes'}, StdModule.travel, {npcHandler = npcHandler, premium = false, level = 0, cost = 100, destination = {x=587, y=544, z=6} })
	travelNode:addChildKeyword({'sim'}, StdModule.travel, {npcHandler = npcHandler, premium = false, level = 0, cost = 100, destination = {x=587, y=544, z=6} })
	
-- Don't forget npcHandler = npcHandler in the parameters. It is required for all StdModule functions!
local travelNode = keywordHandler:addKeyword({'andorhal'}, StdModule.say, {npcHandler = npcHandler, onlyFocus = true, text = 'Voc� deseja ir para Andorhal em troca de 120 gps?'})
	travelNode:addChildKeyword({'yes'}, StdModule.travel, {npcHandler = npcHandler, premium = false, level = 0, cost = 120, destination = {x=1009, y=485, z=6} })
	travelNode:addChildKeyword({'sim'}, StdModule.travel, {npcHandler = npcHandler, premium = false, level = 0, cost = 120, destination = {x=1009, y=485, z=6} })

-- Makes sure the npc reacts when you say hi, bye etc.
npcHandler:addModule(FocusModule:new())