local keywordHandler = KeywordHandler:new()
local npcHandler = NpcHandler:new(keywordHandler)
NpcSystem.parseParameters(npcHandler)
local talkState = {}

function onCreatureAppear(cid)				npcHandler:onCreatureAppear(cid)			end
function onCreatureDisappear(cid) 			npcHandler:onCreatureDisappear(cid)			end
function onCreatureSay(cid, type, msg)			npcHandler:onCreatureSay(cid, type, msg)		end
function onThink()					npcHandler:onThink()					end

local shopModule = ShopModule:new()
npcHandler:addModule(shopModule)

shopModule:addBuyableItem({'blank rune'}, 2260, 15, 1, 'blank rune')
shopModule:addBuyableItem({'destroy field'}, 2261, 45, 1, 'destroy field rune')
shopModule:addBuyableItem({'energy bomb'}, 2262, 590, 1, 'energy bomb rune')
shopModule:addBuyableItem({'explosion'}, 2313, 170, 1, 'explosion rune')
shopModule:addBuyableItem({'firebomb'}, 2305, 480, 1, 'firebomb rune')
shopModule:addBuyableItem({'frasco de mana m�dio'}, 7620, 320, 1, 'frasco de mana m�dio')
shopModule:addBuyableItem({'great fireball'}, 2304, 120, 1, 'great fireball rune')
shopModule:addBuyableItem({'hailstorm rod', 'hailstorm'}, 2183, 15000, 'hailstorm rod')
shopModule:addBuyableItem({'heavy magic missile'}, 2311, 300, 10, 'heavy magic missile rune')
shopModule:addBuyableItem({'livro n�vel 1'}, 8903, 1000, 1, 'livro n�vel 1')
shopModule:addBuyableItem({'livro n�vel 2'}, 8901, 8000, 1, 'livro n�vel 2')
shopModule:addBuyableItem({'livro n�vel 3'}, 8900, 15000, 1, 'livro n�vel 3')
shopModule:addBuyableItem({'magic wall'}, 2293, 410, 1, 'magic wall rune')
shopModule:addBuyableItem({'moonlight rod', 'moonlight'}, 2186, 1000, 'moonlight rod')
shopModule:addBuyableItem({'necrotic rod', 'necrotic'}, 2185, 5000, 'necrotic rod')
shopModule:addBuyableItem({'paralyze'}, 2278, 800, 1, 'paralyze rune')
shopModule:addBuyableItem({'poison bomb'}, 2286, 370, 1, 'poison bomb rune')
shopModule:addBuyableItem({'snakebite rod', 'snakebite'}, 2182, 500, 'snakebite rod')
shopModule:addBuyableItem({'sudden death'}, 2268, 160, 3, 'sudden death rune')
shopModule:addBuyableItem({'terra rod', 'terra'}, 2181, 10000, 'terra rod')
shopModule:addBuyableItem({'ultimate healing'}, 2273, 65, 1, 'ultimate healing rune')
shopModule:addBuyableItem({'wand of cosmic energy', 'cosmic energy'}, 2189, 10000, 'wand of cosmic energy')
shopModule:addBuyableItem({'wand of decay', 'decay'}, 2188, 5000, 'wand of decay')
shopModule:addBuyableItem({'wand of dragonbreath', 'dragonbreath'}, 2191, 1000, 'wand of dragonbreath')
shopModule:addBuyableItem({'wand of vortex', 'vortex'}, 2190, 500, 'wand of vortex')
shopModule:addBuyableItem({'wand of inferno', 'inferno'}, 2187, 15000, 'wand of inferno')
shopModule:addBuyableItem({'frasco de mana m�dio vazio', 'm�dio'}, 7636, 50, 'frasco de mana m�dio vazio')
shopModule:addBuyableItem({'frasco de mana grande vazio', 'grande'}, 7634, 100, 'frasco de mana grande vazio')
shopModule:addBuyableItem({'frasco de mana gigante vazio', 'gigante'}, 7635, 200, 'frasco de mana gigante vazio')
shopModule:addBuyableItem({'frasco de mana m�dio'}, 7620, 2000, 1, 'frasco de mana m�dio')
shopModule:addBuyableItem({'frasco de mana grande'}, 7589, 5000, 1, 'frasco de mana grande')
shopModule:addBuyableItem({'frasco de mana gigante'}, 7590, 10000, 1, 'frasco de mana gigante')

function creatureSayCallback(cid, type, msg)
	if(not npcHandler:isFocused(cid)) then
		return false
	end

	local talkUser = NPCHANDLER_CONVBEHAVIOR == CONVERSATION_DEFAULT and 0 or cid

	local items = {[1] = 2190, [2] = 2182, [5] = 2190, [6] = 2182}
	if(msgcontains(msg, 'first rod') or msgcontains(msg, 'first wand')) then
		if(isSorcerer(cid) or isDruid(cid)) then
			if(getPlayerStorageValue(cid, 30002) == -1) then
				selfSay('So you ask me for a {' .. getItemNameById(items[getPlayerVocation(cid)]) .. '} to begin your advanture?', cid)
				talkState[talkUser] = 1
			else
				selfSay('What? I have already gave you one {' .. getItemNameById(items[getPlayerVocation(cid)]) .. '}!', cid)
			end
		else
			selfSay('Sorry, you aren\'t a druid either a sorcerer.', cid)
		end
	elseif(msgcontains(msg, 'yes')) then
		if(talkState[talkUser] == 1) then
			doPlayerAddItem(cid, items[getPlayerVocation(cid)], 1)
			selfSay('Here you are young adept, take care yourself.', cid)
			setPlayerStorageValue(cid, 30002, 1)
		end
		talkState[talkUser] = 0
	elseif(msgcontains(msg, 'no') and isInArray({1}, talkState[talkUser]) == TRUE) then
		selfSay('Ok then.', cid)
		talkState[talkUser] = 0
	end

	return true
end

npcHandler:setCallback(CALLBACK_MESSAGE_DEFAULT, creatureSayCallback)
npcHandler:addModule(FocusModule:new())
