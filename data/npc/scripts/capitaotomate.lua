local keywordHandler = KeywordHandler:new()
local npcHandler = NpcHandler:new(keywordHandler)
NpcSystem.parseParameters(npcHandler)
 
        
        
-- OTServ event handling functions start
function onCreatureAppear(cid)				npcHandler:onCreatureAppear(cid) end
function onCreatureDisappear(cid) 			npcHandler:onCreatureDisappear(cid) end
function onCreatureSay(cid, type, msg) 	npcHandler:onCreatureSay(cid, type, msg) end
function onThink() 						npcHandler:onThink() end
-- OTServ event handling functions end
   

	
-- Don't forget npcHandler = npcHandler in the parameters. It is required for all StdModule functions!
local travelNode = keywordHandler:addKeyword({'wisland'}, StdModule.say, {npcHandler = npcHandler, onlyFocus = true, text = 'Voc� deseja ir para Wisland em troca de 50 gps?'})
	travelNode:addChildKeyword({'yes'}, StdModule.travel, {npcHandler = npcHandler, premium = false, level = 0, cost = 50, destination = {x=587, y=544, z=6} })
	travelNode:addChildKeyword({'sim'}, StdModule.travel, {npcHandler = npcHandler, premium = false, level = 0, cost = 50, destination = {x=587, y=544, z=6} })

-- Don't forget npcHandler = npcHandler in the parameters. It is required for all StdModule functions!
local travelNode = keywordHandler:addKeyword({'hell'}, StdModule.say, {npcHandler = npcHandler, onlyFocus = true, text = 'Voc� deseja ir para Hell em troca de 250 gps?'})
	travelNode:addChildKeyword({'yes'}, StdModule.travel, {npcHandler = npcHandler, premium = false, level = 0, cost = 250, destination = {x=1023, y=1042, z=6} })
	travelNode:addChildKeyword({'sim'}, StdModule.travel, {npcHandler = npcHandler, premium = false, level = 0, cost = 250, destination = {x=1023, y=1042, z=6} })

-- Don't forget npcHandler = npcHandler in the parameters. It is required for all StdModule functions!
local travelNode = keywordHandler:addKeyword({'lost island'}, StdModule.say, {npcHandler = npcHandler, onlyFocus = true, text = 'Voc� deseja ir para Lost Island em troca de 10000 gps?'})
	travelNode:addChildKeyword({'yes'}, StdModule.travel, {npcHandler = npcHandler, premium = false, level = 0, cost = 1000, destination = {x=1327, y=804, z=7} })
	travelNode:addChildKeyword({'sim'}, StdModule.travel, {npcHandler = npcHandler, premium = false, level = 0, cost = 1000, destination = {x=1327, y=804, z=7} })
	
	-- Makes sure the npc reacts when you say hi, bye etc.
npcHandler:addModule(FocusModule:new())