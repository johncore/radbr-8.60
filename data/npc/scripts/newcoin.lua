local keywordHandler = KeywordHandler:new()
local npcHandler = NpcHandler:new(keywordHandler)
NpcSystem.parseParameters(npcHandler)
local talkState = {}
function onCreatureAppear(cid) npcHandler:onCreatureAppear(cid) end
function onCreatureDisappear(cid) npcHandler:onCreatureDisappear(cid) end
function onCreatureSay(cid, type, msg) npcHandler:onCreatureSay(cid, type, msg) end
function onThink() npcHandler:onThink() end
function creatureSayCallback(cid, type, msg)
if(not npcHandler:isFocused(cid)) then
return false
end
local talkUser = NPCHANDLER_CONVbehavior == CONVERSATION_DEFAULT and 0 or cid
local shopWindow = {}
local moeda = 2328 -- [iD DA MOEDA]
local t = {
 [12610] = {price = 100}, -- [ID do item] e o pre�o que ele ir� custar em points.
 [8925] = {price = 100},
 [8849] = {price = 100},
 [7881] = {price = 100},
 [7879] = {price = 100},
 [7958] = {price = 100},
 [12605] = {price = 80},
 [9933] = {price = 80},
 [8266] = {price = 55},
 }
local onBuy = function(cid, item, subType, amount, ignoreCap)
if  t[item] and not doPlayerRemoveItem(cid, moeda, t[item].price) then
 selfSay("voc� n�o tem "..t[item].price.." "..getItemNameById(moeda), cid)
else
doPlayerAddItem(cid, item)
selfSay("aqui est� seu item!", cid)
  end
return true
end
if (msgcontains(msg, 'trade') or msgcontains(msg, 'TRADE'))then
for var, ret in pairs(t) do
table.insert(shopWindow, {id = var, subType = 0, buy = ret.price, sell = 0, name = getItemNameById(var)})
end
openShopWindow(cid, shopWindow, onBuy, onSell)
end
return true
end
npcHandler:setCallback(CALLBACK_MESSAGE_DEFAULT, creatureSayCallback)
npcHandler:addModule(FocusModule:new())