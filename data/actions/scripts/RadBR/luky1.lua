function onUse(cid, item, frompos, item2, topos)

item1pos = {x=570, y=531, z=7, stackpos=255} --item1pos
item2pos = {x=571, y=531, z=7, stackpos=255} --item2pos
item3pos = {x=570, y=531, z=7, stackpos=255} --item3pos
item1 = getThingfromPos(item1pos)
item2 = getThingfromPos(item2pos)
item3 = getThingfromPos(item3pos)

-- War Hammer --
if item.itemid == 1945 and item1.itemid == 2391 and item2.itemid == 6550 or item1.itemid == 6550 and item2.itemid == 2391 then
if math.random(1, 100) >= 1 and math.random(1, 100) <= 75 then
doRemoveItem(item1.uid,1)
doRemoveItem(item2.uid,1)
doSendMagicEffect(item3pos,29)
doCreateItem(7758,1, item3pos)
doPlayerSendTextMessage(cid,22,'A arma foi blessada com sucesso.')
elseif item.uid == 1991 and item.itemid == 1946 then
doTransformItem(item.uid,item.itemid-1)
else
doPlayerSendTextMessage(cid,22,'A arma quebrou.')
doRemoveItem(item1.uid,1)
doRemoveItem(item2.uid,1)
doSendMagicEffect(item3pos,3)
end
end


-- Spike Sword --
if item.itemid == 1945 and item1.itemid == 2383 and item2.itemid == 6550 or item1.itemid == 6550 and item2.itemid == 2383 then
if math.random(1, 100) >= 1 and math.random(1, 100) <= 75 then
doRemoveItem(item1.uid,1)
doRemoveItem(item2.uid,1)
doSendMagicEffect(item3pos,29)
doCreateItem(7744,1, item3pos)
doPlayerSendTextMessage(cid,22,'A arma foi blessada com sucesso.')
elseif item.uid == 1991 and item.itemid == 1946 then
doTransformItem(item.uid,item.itemid-1)
else
doPlayerSendTextMessage(cid,22,'A arma quebrou.')
doRemoveItem(item1.uid,1)
doRemoveItem(item2.uid,1)
doSendMagicEffect(item3pos,3)
end
end

-- War axe --
if item.itemid == 1945 and item1.itemid == 2454 and item2.itemid == 6550 or item1.itemid == 6550 and item2.itemid == 2454 then
if math.random(1, 100) >= 1 and math.random(1, 100) <= 75 then
doRemoveItem(item1.uid,1)
doRemoveItem(item2.uid,1)
doSendMagicEffect(item3pos,29)
doCreateItem(7753,1, item3pos)
doPlayerSendTextMessage(cid,22,'A arma foi blessada com sucesso.')
elseif item.uid == 1991 and item.itemid == 1946 then
doTransformItem(item.uid,item.itemid-1)
else
doPlayerSendTextMessage(cid,22,'A arma quebrou.')
doRemoveItem(item1.uid,1)
doRemoveItem(item2.uid,1)
doSendMagicEffect(item3pos,3)
end
end

 -- Crystal Mace --
if item.itemid == 1945 and item1.itemid == 2445 and item2.itemid == 6550 or item1.itemid == 6550 and item2.itemid == 2445 then
if math.random(1, 100) >= 1 and math.random(1, 100) <= 75 then
doRemoveItem(item1.uid,1)
doRemoveItem(item2.uid,1)
doSendMagicEffect(item3pos,29)
doCreateItem(7755,1, item3pos)
doPlayerSendTextMessage(cid,22,'A arma foi blessada com sucesso.')
elseif item.uid == 1991 and item.itemid == 1946 then
doTransformItem(item.uid,item.itemid-1)
else
doPlayerSendTextMessage(cid,22,'A arma quebrou.')
doRemoveItem(item1.uid,1)
doRemoveItem(item2.uid,1)
doSendMagicEffect(item3pos,3)
return TRUE
end
end

-- Dragon Slayer --
if item.itemid == 1945 and item1.itemid == 7402 and item2.itemid == 6550 or item1.itemid == 6550 and item2.itemid == 7402 then
if math.random(1, 100) >= 1 and math.random(1, 100) <= 75 then
doRemoveItem(item1.uid,1)
doRemoveItem(item2.uid,1)
doSendMagicEffect(item3pos,29)
doCreateItem(7748,1, item3pos)
doPlayerSendTextMessage(cid,22,'A arma foi blessada com sucesso.')
elseif item.uid == 1991 and item.itemid == 1946 then
doTransformItem(item.uid,item.itemid-1)
else
doPlayerSendTextMessage(cid,22,'A arma quebrou.')
doRemoveItem(item1.uid,1)
doRemoveItem(item2.uid,1)
doSendMagicEffect(item3pos,3)
end
end

-- Divine Club --
if item.itemid == 1945 and item1.itemid == 7392 and item2.itemid == 6550 or item1.itemid == 6550 and item2.itemid == 7392 then
if math.random(1, 100) >= 1 and math.random(1, 100) <= 75 then
doRemoveItem(item1.uid,1)
doRemoveItem(item2.uid,1)
doSendMagicEffect(item3pos,29)
doCreateItem(7757,1, item3pos)
doPlayerSendTextMessage(cid,22,'A arma foi blessada com sucesso.')
elseif item.uid == 1991 and item.itemid == 1946 then
doTransformItem(item.uid,item.itemid-1)
else
doPlayerSendTextMessage(cid,22,'A arma quebrou.')
doRemoveItem(item1.uid,1)
doRemoveItem(item2.uid,1)
doSendMagicEffect(item3pos,3)
end
end

-- Barbarian Axe --
if item.itemid == 1945 and item1.itemid == 2429 and item2.itemid == 6550 or item1.itemid == 6550 and item2.itemid == 2429 then
if math.random(1, 100) >= 1 and math.random(1, 100) <= 75 then
doRemoveItem(item1.uid,1)
doRemoveItem(item2.uid,1)
doSendMagicEffect(item3pos,29)
doCreateItem(7749,1, item3pos)
doPlayerSendTextMessage(cid,22,'A arma foi blessada com sucesso.')
elseif item.uid == 1991 and item.itemid == 1946 then
doTransformItem(item.uid,item.itemid-1)
else
doPlayerSendTextMessage(cid,22,'A arma quebrou.')
doRemoveItem(item1.uid,1)
doRemoveItem(item2.uid,1)
doSendMagicEffect(item3pos,3)
end
end

-- Knight Axe --
if item.itemid == 1945 and item1.itemid == 2430 and item2.itemid == 6550 or item1.itemid == 6550 and item2.itemid == 2430 then
if math.random(1, 100) >= 1 and math.random(1, 100) <= 75 then
doRemoveItem(item1.uid,1)
doRemoveItem(item2.uid,1)
doSendMagicEffect(item3pos,29)
doCreateItem(7750,1, item3pos)
doPlayerSendTextMessage(cid,22,'A arma foi blessada com sucesso.')
elseif item.uid == 1991 and item.itemid == 1946 then
doTransformItem(item.uid,item.itemid-1)
else
doPlayerSendTextMessage(cid,22,'A arma quebrou.')
doRemoveItem(item1.uid,1)
doRemoveItem(item2.uid,1)
doSendMagicEffect(item3pos,3)
end
end
end