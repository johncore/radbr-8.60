function onUse(cid, item, fromPos, itemEx, toPos)

-- [[INICIO CONFIG - Beyond Sky]]
monstros =
{
[1] = {chance = 25, name = "Demon"},				 --[[Monstro do shirine fire e item fire]]
[2] = {chance = 25, name = "Fire Elemental"},		--[[Monstro do shirine fire e item fire]]
[3] = {chance = 25, name = "Hellfire Fighter"},	  --[[Monstro do shirine fire e item fire]]
[4] = {chance = 1, name = "Dourado"},			   --[[Monstro do shirine fire e item fire]]
[5] = {chance = 25, name = "Gigante de Gelo"},	   --[[Monstro do shirine ice e item ice]]
[6] = {chance = 25, name = "Frost Dragon"},		  --[[Monstro do shirine ice e item ice]]
[7] = {chance = 25, name = "Papao"},		--[[Monstro do shirine ice e item ice]]
[8] = {chance = 1, name = "Orghus"},				--[[Monstro do shirine ice e item ice]]
[9] = {chance = 25, name = "Multi"},				 --[[Monstro do shirine tera e item tera]]
[10] = {chance = 25, name = "Juggernaut"},		   --[[Monstro do shirine tera e item tera]]
[11] = {chance = 25, name = "Hydra"},				--[[Monstro do shirine tera e item tera]]
[12] = {chance = 1, name = "Guardiao da Montanha"}, --[[Monstro do shirine tera e item tera]]
[13] = {chance = 25, name = "Minotaur Mage"},		--[[Monstro do shirine energy e item energ]]
[14] = {chance = 25, name = "Dragon Guardian"},	  --[[Monstro do shirine energy e item energ]]
[15] = {chance = 25, name = "Thunder Dragon"},	   --[[Monstro do shirine energy e item energ]]
[16] = {chance = 1, name = "Nephthys"}			   --[[Monstro do shirine energy e item energ]]
}

local uniqid = 9999 --[[UNIQUE ID DA ALAVANCA - Beyond Sky]]

local ItemIdFire = 6550		  --[[Item do shrine Fire]]
local ItemIdIce = 6551		   --[[Item do shrine Ice]]
local ItemIdTera = 6549		  --[[Item do shrine Tera]]
local ItemIdEnergy = 6548		--[[Item do shrine Energy]]

local PosNameFire = {x=324, y=1265, z=9}		 --[[Pos do monster da shrine Fire]]
local PosNameIce = {x=324, y=1258, z=9}		  --[[Pos do monster da shrine Ice]]
local PosNameTera = {x=324, y=1255, z=9}		 --[[Pos do monster da shrine Tera]]
local PosNameEnergy = {x=324, y=1267, z=9}	   --[[Pos do monster da shrine Energy]]

local PosItem = {x=350, y=1261, z=9, stackpos = 255}	  --[[Posi��o do item que vai ser preciso]]
local getitem = getThingfromPos(PosItem)
-- [[FIM CONFIG - Beyond Sky]]

	for i = 1, 4 do
		if item.uid == uniqid and getitem.itemid == ItemIdFire and item.itemid == 9825 and (monstros[i].chance >= math.random(1, 100)) then
doSummonCreature(monstros[i].name, PosNameFire)
doRemoveItem(getitem.uid,1)
doCreatureSay(cid, "A m�quina de summon criou um "..monstros[i].name..".", TALKTYPE_ORANGE_1)
doSendMagicEffect(PosItem, 13)	

elseif item.uid == uniqid and getitem.itemid == ItemIdIce and item.itemid == 9825 and (monstros[i+4].chance >= math.random(1, 100)) then
   doSummonCreature(monstros[i+4].name, PosNameIce)
doRemoveItem(getitem.uid,1)
doCreatureSay(cid, "A m�quina de summon criou um "..monstros[i+4].name..".", TALKTYPE_ORANGE_1)
doSendMagicEffect(PosItem, 13)  

elseif item.uid == uniqid and getitem.itemid == ItemIdTera and item.itemid == 9825 and (monstros[i+8].chance >= math.random(1, 100)) then
			doSummonCreature(monstros[i+8].name, PosNameTera)
doRemoveItem(getitem.uid,1)
doCreatureSay(cid, "A m�quina de summon criou um "..monstros[i+8].name..".", TALKTYPE_ORANGE_1)
doSendMagicEffect(PosItem, 13)  

elseif item.uid == uniqid and getitem.itemid == ItemIdEnergy and item.itemid == 9825 and (monstros[i+12].chance >= math.random(1, 100)) then
			doSummonCreature(monstros[i+12].name, PosNameEnergy)
doRemoveItem(getitem.uid,1)
doCreatureSay(cid, "A m�quina de summon criou um "..monstros[i+12].name..".", TALKTYPE_ORANGE_1)
doSendMagicEffect(PosItem, 13)  

elseif item.uid == uniqid and item.itemid == 9826 then
doTransformItem(item.uid, item.itemid - 1)
return TRUE
		end
	end
end