local cfg = {
          storage = 9999 -- Storage para o jogador n�o fazer mais que uma vez a mesma quest
          reward = 2507 -- ID do item que o jogador ganhar� como recompensa
          count = 1 -- Quantidade do item que o jogador ganhar� como recompensa
          pos = {x=543, y=521, z=7} -- Posi��o do local que o jogador ser� teletransportado
}
function onUse(cid, item, frompos, item2, topos)
          if getPlayerStorageValue(cid, cfg.storage) <= 0 then
                    doPlayerAddItem(cid, cfg.reward, cfg.count)
                    doTeleportThing(cid, cfg.pos)
                    doPlayerSendTextMessage(cid, 18, "Ganhou Uma King Legs ".. getItemNameById(cfg.reward) ..".")
          else
                    doPlayerSendCancel(cid, "Voc� Ja Fez Essa Quest")
          end
          return TRUE
end