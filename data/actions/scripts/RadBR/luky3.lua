function onUse(cid, item, frompos, item2, topos)

item1pos = {x=573, y=531, z=7, stackpos=255} --item1pos
item2pos = {x=574, y=531, z=7, stackpos=255} --item2pos
item3pos = {x=573, y=531, z=7, stackpos=255} --item3pos
item1 = getThingfromPos(item1pos)
item2 = getThingfromPos(item2pos)
item3 = getThingfromPos(item3pos)

-- War Hammer --
if item.itemid == 1945 and item1.itemid == 7777 and item2.itemid == 6549 or item1.itemid == 6549 and item2.itemid == 7777 then
if math.random(1, 100) >= 1 and math.random(1, 100) <= 25 then
doRemoveItem(item1.uid,1)
doRemoveItem(item2.uid,1)
doSendMagicEffect(item3pos,29)
doCreateItem(7868,1, item3pos)
doPlayerSendTextMessage(cid,22,'A arma foi blessada com sucesso.')
elseif item.uid == 1993 and item.itemid == 1946 then
doTransformItem(item.uid,item.itemid-1)
else
doPlayerSendTextMessage(cid,22,'A arma quebrou.')
doRemoveItem(item1.uid,1)
doRemoveItem(item2.uid,1)
doSendMagicEffect(item3pos,3)
end
end

-- Spike Sword --
if item.itemid == 1945 and item1.itemid == 7763 and item2.itemid == 6549 or item1.itemid == 6549 and item2.itemid == 7763 then
if math.random(1, 100) >= 1 and math.random(1, 100) <= 25 then
doRemoveItem(item1.uid,1)
doRemoveItem(item2.uid,1)
doSendMagicEffect(item3pos,29)
doCreateItem(7854,1, item3pos)
doPlayerSendTextMessage(cid,22,'A arma foi blessada com sucesso.')
elseif item.uid == 1993 and item.itemid == 1946 then
doTransformItem(item.uid,item.itemid-1)
else
doPlayerSendTextMessage(cid,22,'A arma quebrou.')
doRemoveItem(item1.uid,1)
doRemoveItem(item2.uid,1)
doSendMagicEffect(item3pos,3)
end
end

-- War axe --
if item.itemid == 1945 and item1.itemid == 7772 and item2.itemid == 6549 or item1.itemid == 6549 and item2.itemid == 7772 then
if math.random(1, 100) >= 1 and math.random(1, 100) <= 25 then
doRemoveItem(item1.uid,1)
doRemoveItem(item2.uid,1)
doSendMagicEffect(item3pos,29)
doCreateItem(7863,1, item3pos)
doPlayerSendTextMessage(cid,22,'A arma foi blessada com sucesso.')
elseif item.uid == 1993 and item.itemid == 1946 then
doTransformItem(item.uid,item.itemid-1)
else
doPlayerSendTextMessage(cid,22,'A arma quebrou.')
doRemoveItem(item1.uid,1)
doRemoveItem(item2.uid,1)
doSendMagicEffect(item3pos,3)
end
end

 -- Crystal Mace --
if item.itemid == 1945 and item1.itemid == 7774 and item2.itemid == 6549 or item1.itemid == 6549 and item2.itemid == 7774 then
if math.random(1, 100) >= 1 and math.random(1, 100) <= 25 then
doRemoveItem(item1.uid,1)
doRemoveItem(item2.uid,1)
doSendMagicEffect(item3pos,29)
doCreateItem(7865,1, item3pos)
doPlayerSendTextMessage(cid,22,'A arma foi blessada com sucesso.')
elseif item.uid == 1993 and item.itemid == 1946 then
doTransformItem(item.uid,item.itemid-1)
else
doPlayerSendTextMessage(cid,22,'A arma quebrou.')
doRemoveItem(item1.uid,1)
doRemoveItem(item2.uid,1)
doSendMagicEffect(item3pos,3)
end
end

-- Dragon Slayer --
if item.itemid == 1945 and item1.itemid == 7767 and item2.itemid == 6549 or item1.itemid == 6549 and item2.itemid == 7767 then
if math.random(1, 100) >= 1 and math.random(1, 100) <= 25 then
doRemoveItem(item1.uid,1)
doRemoveItem(item2.uid,1)
doSendMagicEffect(item3pos,29)
doCreateItem(7858,1, item3pos)
doPlayerSendTextMessage(cid,22,'A arma foi blessada com sucesso.')
elseif item.uid == 1993 and item.itemid == 1946 then
doTransformItem(item.uid,item.itemid-1)
else
doPlayerSendTextMessage(cid,22,'A arma quebrou.')
doRemoveItem(item1.uid,1)
doRemoveItem(item2.uid,1)
doSendMagicEffect(item3pos,3)
end
end

-- Divine Club --
if item.itemid == 1945 and item1.itemid == 7776 and item2.itemid == 6549 or item1.itemid == 6549 and item2.itemid == 7776 then
if math.random(1, 100) >= 1 and math.random(1, 100) <= 25 then
doRemoveItem(item1.uid,1)
doRemoveItem(item2.uid,1)
doSendMagicEffect(item3pos,29)
doCreateItem(7867,1, item3pos)
doPlayerSendTextMessage(cid,22,'A arma foi blessada com sucesso.')
elseif item.uid == 1993 and item.itemid == 1946 then
doTransformItem(item.uid,item.itemid-1)
else
doPlayerSendTextMessage(cid,22,'A arma quebrou.')
doRemoveItem(item1.uid,1)
doRemoveItem(item2.uid,1)
doSendMagicEffect(item3pos,3)
end
end

-- Barbarian Axe --
if item.itemid == 1945 and item1.itemid == 7768 and item2.itemid == 6549 or item1.itemid == 6549 and item2.itemid == 7768 then
if math.random(1, 100) >= 1 and math.random(1, 100) <= 25 then
doRemoveItem(item1.uid,1)
doRemoveItem(item2.uid,1)
doSendMagicEffect(item3pos,29)
doCreateItem(7859,1, item3pos)
doPlayerSendTextMessage(cid,22,'A arma foi blessada com sucesso.')
elseif item.uid == 1991 and item.itemid == 1946 then
doTransformItem(item.uid,item.itemid-1)
else
doPlayerSendTextMessage(cid,22,'A arma quebrou.')
doRemoveItem(item1.uid,1)
doRemoveItem(item2.uid,1)
doSendMagicEffect(item3pos,3)
end
end

-- Knight Axe --
if item.itemid == 1945 and item1.itemid == 7769 and item2.itemid == 6549 or item1.itemid == 6549 and item2.itemid == 7769 then
if math.random(1, 100) >= 1 and math.random(1, 100) <= 25 then
doRemoveItem(item1.uid,1)
doRemoveItem(item2.uid,1)
doSendMagicEffect(item3pos,29)
doCreateItem(7860,1, item3pos)
doPlayerSendTextMessage(cid,22,'A arma foi blessada com sucesso.')
elseif item.uid == 1993 and item.itemid == 1946 then
doTransformItem(item.uid,item.itemid-1)
else
doPlayerSendTextMessage(cid,22,'A arma quebrou.')
doRemoveItem(item1.uid,1)
doRemoveItem(item2.uid,1)
doSendMagicEffect(item3pos,3)
end
end
end