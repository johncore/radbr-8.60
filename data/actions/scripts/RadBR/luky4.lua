function onUse(cid, item, frompos, item2, topos)

item1pos = {x=573, y=525, z=7, stackpos=255} --item1pos
item2pos = {x=574, y=525, z=7, stackpos=255} --item2pos
item3pos = {x=573, y=525, z=7, stackpos=255} --item3pos
item1 = getThingfromPos(item1pos)
item2 = getThingfromPos(item2pos)
item3 = getThingfromPos(item3pos)
porcentagem = 85
porcent = math.random(1, 100)

-- War Hammer --
if item.itemid == 1945 and item1.itemid == 7868 and item2.itemid == 6548 or item1.itemid == 6548 and item2.itemid == 7868 then
if math.random(1, 100) >= 1 and math.random(1, 100) <= 15 then
doRemoveItem(item1.uid,1)
doRemoveItem(item2.uid,1)
doSendMagicEffect(item3pos,29)
doCreateItem(7883,1, item3pos)
doPlayerSendTextMessage(cid,22,'A arma foi blessada com sucesso.')
elseif item.uid == 1994 and item.itemid == 1946 then
doTransformItem(item.uid,item.itemid-1)
else
doPlayerSendTextMessage(cid,22,'A arma quebrou.')
doRemoveItem(item1.uid,1)
doRemoveItem(item2.uid,1)
doSendMagicEffect(item3pos,3)
end
end

-- Spike Sword --
if item.itemid == 1945 and item1.itemid == 7854 and item2.itemid == 6548 or item1.itemid == 6548 and item2.itemid == 7854 then
if math.random(1, 100) >= 1 and math.random(1, 100) <= 15 then
doRemoveItem(item1.uid,1)
doRemoveItem(item2.uid,1)
doSendMagicEffect(item3pos,29)
doCreateItem(7869,1, item3pos)
doPlayerSendTextMessage(cid,22,'A arma foi blessada com sucesso.')
elseif item.uid == 1994 and item.itemid == 1946 then
doTransformItem(item.uid,item.itemid-1)
else
doPlayerSendTextMessage(cid,22,'A arma quebrou.')
doRemoveItem(item1.uid,1)
doRemoveItem(item2.uid,1)
doSendMagicEffect(item3pos,3)
end
end

-- War axe --
if item.itemid == 1945 and item1.itemid == 7863 and item2.itemid == 6548 or item1.itemid == 6548 and item2.itemid == 7863 then
if math.random(1, 100) >= 1 and math.random(1, 100) <= 15 then
doRemoveItem(item1.uid,1)
doRemoveItem(item2.uid,1)
doSendMagicEffect(item3pos,29)
doCreateItem(7878,1, item3pos)
doPlayerSendTextMessage(cid,22,'A arma foi blessada com sucesso.')
elseif item.uid == 1994 and item.itemid == 1946 then
doTransformItem(item.uid,item.itemid-1)
else
doPlayerSendTextMessage(cid,22,'A arma quebrou.')
doRemoveItem(item1.uid,1)
doRemoveItem(item2.uid,1)
doSendMagicEffect(item3pos,3)
end
end

 -- Crystal Mace --
if item.itemid == 1945 and item1.itemid == 7865 and item2.itemid == 6548 or item1.itemid == 6548 and item2.itemid == 7865 then
if math.random(1, 100) >= 1 and math.random(1, 100) <= 15 then
doRemoveItem(item1.uid,1)
doRemoveItem(item2.uid,1)
doSendMagicEffect(item3pos,29)
doCreateItem(7880,1, item3pos)
doPlayerSendTextMessage(cid,22,'A arma foi blessada com sucesso.')
elseif item.uid == 1994 and item.itemid == 1946 then
doTransformItem(item.uid,item.itemid-1)
else
doPlayerSendTextMessage(cid,22,'A arma quebrou.')
doRemoveItem(item1.uid,1)
doRemoveItem(item2.uid,1)
doSendMagicEffect(item3pos,3)
end
end

-- Dragon Slayer --
if item.itemid == 1945 and item1.itemid == 7858 and item2.itemid == 6548 or item1.itemid == 6548 and item2.itemid == 7858 then
if math.random(1, 100) >= 1 and math.random(1, 100) <= 15 then
doRemoveItem(item1.uid,1)
doRemoveItem(item2.uid,1)
doSendMagicEffect(item3pos,29)
doCreateItem(7873,1, item3pos)
doPlayerSendTextMessage(cid,22,'A arma foi blessada com sucesso.')
elseif item.uid == 1994 and item.itemid == 1946 then
doTransformItem(item.uid,item.itemid-1)
else
doPlayerSendTextMessage(cid,22,'A arma quebrou.')
doRemoveItem(item1.uid,1)
doRemoveItem(item2.uid,1)
doSendMagicEffect(item3pos,3)
end
end

-- Divine Club --
if item.itemid == 1945 and item1.itemid == 7867 and item2.itemid == 6548 or item1.itemid == 6548 and item2.itemid == 7867 then
if math.random(1, 100) >= 1 and math.random(1, 100) <= 15 then
doRemoveItem(item1.uid,1)
doRemoveItem(item2.uid,1)
doSendMagicEffect(item3pos,29)
doCreateItem(7882,1, item3pos)
doPlayerSendTextMessage(cid,22,'A arma foi blessada com sucesso.')
elseif item.uid == 1994 and item.itemid == 1946 then
doTransformItem(item.uid,item.itemid-1)
else
doPlayerSendTextMessage(cid,22,'A arma quebrou.')
doRemoveItem(item1.uid,1)
doRemoveItem(item2.uid,1)
doSendMagicEffect(item3pos,3)
end
end

-- Barbarian Axe --
if item.itemid == 1945 and item1.itemid == 7859 and item2.itemid == 6548 or item1.itemid == 6548 and item2.itemid == 7859 then
if math.random(1, 100) >= 1 and math.random(1, 100) <= 15 then
doRemoveItem(item1.uid,1)
doRemoveItem(item2.uid,1)
doSendMagicEffect(item3pos,29)
doCreateItem(7874,1, item3pos)
doPlayerSendTextMessage(cid,22,'A arma foi blessada com sucesso.')
elseif item.uid == 1991 and item.itemid == 1946 then
doTransformItem(item.uid,item.itemid-1)
else
doPlayerSendTextMessage(cid,22,'A arma quebrou.')
doRemoveItem(item1.uid,1)
doRemoveItem(item2.uid,1)
doSendMagicEffect(item3pos,3)
end
end

-- Knight Axe --
if item.itemid == 1945 and item1.itemid == 7860 and item2.itemid == 6548 or item1.itemid == 6548 and item2.itemid == 7860 then
if math.random(1, 100) >= 1 and math.random(1, 100) <= 15 then
doRemoveItem(item1.uid,1)
doRemoveItem(item2.uid,1)
doSendMagicEffect(item3pos,29)
doCreateItem(7875,1, item3pos)
doPlayerSendTextMessage(cid,22,'A arma foi blessada com sucesso.')
elseif item.uid == 1994 and item.itemid == 1946 then
doTransformItem(item.uid,item.itemid-1)
else
doPlayerSendTextMessage(cid,22,'A arma quebrou.')
doRemoveItem(item1.uid,1)
doRemoveItem(item2.uid,1)
doSendMagicEffect(item3pos,3)
end
end
end