function onUse(cid, item, frompos, item2, topos)

item1pos = {x=570, y=525, z=7, stackpos=255} --item1pos
item2pos = {x=571, y=525, z=7, stackpos=255} --item2pos
item3pos = {x=570, y=525, z=7, stackpos=255} --item3pos
item1 = getThingfromPos(item1pos)
item2 = getThingfromPos(item2pos)
item3 = getThingfromPos(item3pos)

-- War Hammer --
if item.itemid == 1945 and item1.itemid == 7758 and item2.itemid == 6551 or item1.itemid == 6551 and item2.itemid == 7758 then
if math.random(1, 100) >= 1 and math.random(1, 100) <= 50 then
doRemoveItem(item1.uid,1)
doRemoveItem(item2.uid,1)
doSendMagicEffect(item3pos,29)
doCreateItem(7777,1, item3pos)
doPlayerSendTextMessage(cid,22,'A arma foi blessada com sucesso.')
elseif item.uid == 1992 and item.itemid == 1946 then
doTransformItem(item.uid,item.itemid-1)
else
doPlayerSendTextMessage(cid,22,'A arma quebrou.')
doRemoveItem(item1.uid,1)
doRemoveItem(item2.uid,1)
doSendMagicEffect(item3pos,3)
end
end

-- Spike Sword --
if item.itemid == 1945 and item1.itemid == 7744 and item2.itemid == 6551 or item1.itemid == 6551 and item2.itemid == 7744 then
if math.random(1, 100) >= 1 and math.random(1, 100) <= 50 then
doRemoveItem(item1.uid,1)
doRemoveItem(item2.uid,1)
doSendMagicEffect(item3pos,29)
doCreateItem(7763,1, item3pos)
doPlayerSendTextMessage(cid,22,'A arma foi blessada com sucesso.')
elseif item.uid == 1992 and item.itemid == 1946 then
doTransformItem(item.uid,item.itemid-1)
else
doPlayerSendTextMessage(cid,22,'A arma quebrou.')
doRemoveItem(item1.uid,1)
doRemoveItem(item2.uid,1)
doSendMagicEffect(item3pos,3)
end
end

-- War axe --
if item.itemid == 1945 and item1.itemid == 7753 and item2.itemid == 6551 or item1.itemid == 6551 and item2.itemid == 7753 then
if math.random(1, 100) >= 1 and math.random(1, 100) <= 50 then
doRemoveItem(item1.uid,1)
doRemoveItem(item2.uid,1)
doSendMagicEffect(item3pos,29)
doCreateItem(7772,1, item3pos)
doPlayerSendTextMessage(cid,22,'A arma foi blessada com sucesso.')
elseif item.uid == 1992 and item.itemid == 1946 then
doTransformItem(item.uid,item.itemid-1)
else
doPlayerSendTextMessage(cid,22,'A arma quebrou.')
doRemoveItem(item1.uid,1)
doRemoveItem(item2.uid,1)
doSendMagicEffect(item3pos,3)
end
end

 -- Crystal Mace --
if item.itemid == 1945 and item1.itemid == 7755 and item2.itemid == 6551 or item1.itemid == 6551 and item2.itemid == 7755 then
if math.random(1, 100) >= 1 and math.random(1, 100) <= 50 then
doRemoveItem(item1.uid,1)
doRemoveItem(item2.uid,1)
doSendMagicEffect(item3pos,29)
doCreateItem(7774,1, item3pos)
doPlayerSendTextMessage(cid,22,'A arma foi blessada com sucesso.')
elseif item.uid == 1992 and item.itemid == 1946 then
doTransformItem(item.uid,item.itemid-1)
else
doPlayerSendTextMessage(cid,22,'A arma quebrou.')
doRemoveItem(item1.uid,1)
doRemoveItem(item2.uid,1)
doSendMagicEffect(item3pos,3)
end
end

-- Dragon Slayer --
if item.itemid == 1945 and item1.itemid == 7748 and item2.itemid == 6551 or item1.itemid == 6551 and item2.itemid == 7748 then
if math.random(1, 100) >= 1 and math.random(1, 100) <= 50 then
doRemoveItem(item1.uid,1)
doRemoveItem(item2.uid,1)
doSendMagicEffect(item3pos,29)
doCreateItem(7767,1, item3pos)
doPlayerSendTextMessage(cid,22,'A arma foi blessada com sucesso.')
elseif item.uid == 1992 and item.itemid == 1946 then
doTransformItem(item.uid,item.itemid-1)
else
doPlayerSendTextMessage(cid,22,'A arma quebrou.')
doRemoveItem(item1.uid,1)
doRemoveItem(item2.uid,1)
doSendMagicEffect(item3pos,3)
end
end

-- Divine Club --
if item.itemid == 1945 and item1.itemid == 7757 and item2.itemid == 6551 or item1.itemid == 6551 and item2.itemid == 7757 then
if math.random(1, 100) >= 1 and math.random(1, 100) <= 50 then
doRemoveItem(item1.uid,1)
doRemoveItem(item2.uid,1)
doSendMagicEffect(item3pos,29)
doCreateItem(7776,1, item3pos)
doPlayerSendTextMessage(cid,22,'A arma foi blessada com sucesso.')
elseif item.uid == 1992 and item.itemid == 1946 then
doTransformItem(item.uid,item.itemid-1)
else
doPlayerSendTextMessage(cid,22,'A arma quebrou.')
doRemoveItem(item1.uid,1)
doRemoveItem(item2.uid,1)
doSendMagicEffect(item3pos,3)
end
end

-- Barbarian Axe --
if item.itemid == 1945 and item1.itemid == 7749 and item2.itemid == 6551 or item1.itemid == 6551 and item2.itemid == 7749 then
if math.random(1, 100) >= 1 and math.random(1, 100) <= 50 then
doRemoveItem(item1.uid,1)
doRemoveItem(item2.uid,1)
doSendMagicEffect(item3pos,29)
doCreateItem(7768,1, item3pos)
doPlayerSendTextMessage(cid,22,'A arma foi blessada com sucesso.')
elseif item.uid == 1991 and item.itemid == 1946 then
doTransformItem(item.uid,item.itemid-1)
else
doPlayerSendTextMessage(cid,22,'A arma quebrou.')
doRemoveItem(item1.uid,1)
doRemoveItem(item2.uid,1)
doSendMagicEffect(item3pos,3)
end
end

-- Knight Axe --
if item.itemid == 1945 and item1.itemid == 7750 and item2.itemid == 6551 or item1.itemid == 6551 and item2.itemid == 7750 then
if math.random(1, 100) >= 1 and math.random(1, 100) <= 50 then
doRemoveItem(item1.uid,1)
doRemoveItem(item2.uid,1)
doSendMagicEffect(item3pos,29)
doCreateItem(7769,1, item3pos)
doPlayerSendTextMessage(cid,22,'A arma foi blessada com sucesso.')
elseif item.uid == 1992 and item.itemid == 1946 then
doTransformItem(item.uid,item.itemid-1)
else
doPlayerSendTextMessage(cid,22,'A arma quebrou.')
doRemoveItem(item1.uid,1)
doRemoveItem(item2.uid,1)
doSendMagicEffect(item3pos,3)
end
end
end