function onUse(cid, item, fromPosition, itemEx, toPosition)
	if(item.actionid > 30020 and item.actionid < 30100) then
		local townId = (item.actionid - 30020)
		doPlayerSetTown(cid, townId)
		doPlayerSendTextMessage(cid, MESSAGE_STATUS_CONSOLE_BLUE, "Voc� agora � morador de " .. getTownName(townId) .. ".")
	end

	return true
end
