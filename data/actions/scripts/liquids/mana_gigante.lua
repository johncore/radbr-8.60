local MIN = 1000
local MAX = 1300
local EMPTY_POTION = 7590
local RANGE = -1

local exhaust = createConditionObject(CONDITION_EXHAUST)
setConditionParam(exhaust, CONDITION_PARAM_TICKS, (getConfigInfo('timeBetweenExActions') - 100))

function onUse(cid, item, fromPosition, itemEx, toPosition)
	if isPlayer(itemEx.uid) == FALSE then
		return FALSE
	end

	if hasCondition(cid, CONDITION_EXHAUST_HEAL) == TRUE then
		doPlayerSendDefaultCancel(cid, RETURNVALUE_YOUAREEXHAUSTED)
		return TRUE
	end
	
	if(RANGE > 0 and cid ~= itemEx.uid and getDistanceBetween(getThingPosition(cid), getThingPosition(itemEx.uid)) > RANGE) then
		return FALSE
	end

	if doPlayerAddMana(itemEx.uid, math.random(MIN, MAX)) == LUA_ERROR then
		return FALSE
	end

	doAddCondition(cid, exhaust)
	doSendMagicEffect(getThingPos(itemEx.uid), CONST_ME_MAGIC_BLUE)
	doCreatureSay(itemEx.uid, "Aaaah..!", TALKTYPE_ORANGE_1)
	doRemoveItem(item.uid, 1)
        doPlayerAddItem(cid, EMPTY_POTION, 1)
        pot_count = getPlayerItemCount(cid, EMPTY_POTION)
        doPlayerRemoveItem(cid, EMPTY_POTION, pot_count)
        doPlayerAddItem(cid, EMPTY_POTION, pot_count)
        return TRUE
end
