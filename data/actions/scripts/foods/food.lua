local FOODS, MAX_FOOD = {
	[2328] = {84, "Gulp."},  [2362] = {48, "Ecaa!"}, [2666] = {180, "Nhuum, coxinha.."}, [2667] = {144, "Peixe gostoso :D~"},
	[2668] = {120, "Mmmm."}, [2669] = {204, "Peixe gostoso :D~"}, [2670] = {48, "Urgh, esqueci a casca!"}, [2671] = {360, "Bife bom!"},
	[2672] = {720, "Ser� que tava podre?"}, [2673] = {60, "Yum."}, [2674] = {72, "Nhac, Nhac."}, [2675] = {156, "Yum."},
	[2676] = {96, "Huum, NANANA! :D"}, [2677] = {12, "Nhaaamm!"}, [2678] = {216, "�guinha de coco! :)"}, [2679] = {12, "Nhaaamm!"},
	[2680] = {24, "Yum."}, [2681] = {108, "AH! Engoli caro�o!"}, [2682] = {240, "Yum."}, [2683] = {204, "Munch."},
	[2684] = {60, "Ecaa!"}, [2685] = {72, "Munch."}, [2686] = {108, "Crunch."}, [2687] = {24, "Crunch."},
	[2688] = {24, "Mmmm."}, [2689] = {120, "S� faltou a mortadela..xD"}, [2690] = {72, "S� faltou a gel�ia.."}, [2691] = {96, "Cad� a manteiga? Urgh.."},
	[2695] = {72, "Arrrgh, eca!!"}, [2696] = {108, "Nham nham.."}, [8112] = {108, "Urgh."}, [2769] = {60, "Crunch."}, [2787] = {108, "Nhac, hmm."},
	[2788] = {48, "Nhac, hmm."}, [2789] = {264, "Nhac, hmm."}, [2790] = {360, "Nhac, hmm."}, [2791] = {108, "Nhac, hmm."},
	[2792] = {72, "Nhac, hmm."}, [2793] = {144, "Nhac, hmm."}, [2794] = {36, "Nhac, hmm."}, [2795] = {432, "Nhac, hmm."},
	[2796] = {300, "Nhac, hmm."}, [5097] = {48, "Yum."}, [5678] = {96, "Gulp."}, [6125] = {96, "Gulp."},
	[6278] = {120, "Mmmm."}, [6279] = {180, "Mmmm."}, [6393] = {144, "Mmmm."}, [6394] = {180, "Mmmm."},
	[6501] = {240, "Mmmm."}, [6569] = {12, "Mmmm."}, [6574] = {60, "Mmmm."},
	[7158] = {300, "Este peixe fez me sentir melhor :D"}, [7159] = {180, "Munch."}, [7372] = {0, "Yummy."}, [7373] = {0, "Yummy."},
	[7374] = {0, "Yummy."},  [7375] = {0, "Yummy."}, [7376] = {0, "Yummy."}, [7377] = {0, "Yummy."},
	[7963] = {720, "Munch."},  [8838] = {120, "Gulp."}, [8839] = {60, "Yum."}, [8840] = {12, "Yum."},
	[8841] = {12, "Urgh."}, [8842] = {84, "Munch."}, [8843] = {60, "Crunch."}, [8844] = {12, "Gulp."},
	[8845] = {60, "Munch."}, [8847] = {132, "Yum."}, [9114] = {60, "Crunch."}, [9005] = {88, "Slurp."}, 
	[7245] = {84, "Munch."}, [9996] = {0, "Slurp."},
	[10454] = {0, "Your head begins to feel better."}
}, 1200

function onUse(cid, item, fromPosition, itemEx, toPosition)
	if(item.itemid == 6280) then
		if(fromPosition.x == CONTAINER_POSITION) then
			fromPosition = getThingPosition(cid)
		end

		doCreatureSay(cid, getPlayerName(cid) .. " apagou a vela.", TALKTYPE_MONSTER)
		doTransformItem(item.uid, item.itemid - 1)

		doSendMagicEffect(fromPosition, CONST_ME_POFF)
		return true
	end

	local food = FOODS[item.itemid]
	if(food == nil) then
		return false
	end

	local size = food[1]
	if(getPlayerFood(cid) + size > MAX_FOOD) then
		doPlayerSendCancel(cid, "Voc� est� satisfeito.")
		return true
	end

	doPlayerFeed(cid, size)
	doRemoveItem(item.uid, 1)

	doCreatureSay(cid, food[2], TALKTYPE_MONSTER)
	return true
end
